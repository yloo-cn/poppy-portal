const svgtofont = require('svgtofont');
const path = require('path');
const pkg = require('../package.json');

const options = {
  src: path.resolve(process.cwd(), 'src/icons/fonts'), // svg 图标目录路径
  dist: path.resolve(process.cwd(), 'public/fonts'), // 输出到指定目录中
  styleTemplates: path.resolve(process.cwd(), 'config/templates'),
  fontName: 'fc', // 设置字体名称
  css: {
    fontSize: '1em',
  },
  // 生成字体文件
  generateInfoData: true,
  emptyDist: true,
  classNamePrefix: 'fc',
  startNumber: 20000, // unicode起始编号
  svgicons2svgfont: {
    fontHeight: 1000,
    normalize: true,
  },
  typescript: true,
  website: {
    title: 'icon',
    logo: '',
    version: pkg.version,
    meta: {
      description: '',
      keywords: '',
    },
    description: ``,
    links: [
      {
        title: 'Font Class',
        url: 'index.html',
      },
      {
        title: 'Unicode',
        url: 'unicode.html',
      },
    ],
  },
};

svgtofont(options).then(() => {
  console.info('done!');
});

// async function creatFont() {
//   const unicodeObject = await createSVG(options);
//   const ttf = await createTTF(options); // SVG Font => TTF
//   await createEOT(options, ttf); // TTF => EOT
//   await createWOFF(options, ttf); // TTF => WOFF
//   await createWOFF2(options, ttf); // TTF => WOFF2
//   await createSvgSymbol(options); // SVG Files => SVG Symbol
//   await createHTML(options, ttf);
// }

// creatFont();

// const outPath = generateIconsSource(options);
