// svgo.config.js
module.exports = {
  // Ensures the best optimization.
  multipass: true,
  js2svg: {
    // Beutifies the SVG output instead of
    // stripping all white space.
    pretty: true,
    indent: 2,
  },
  plugins: [
    {
      name: 'preset-default',
      params: {
        overrides: {
          // customize default plugin options
          // inlineStyles: {
          //   onlyMatchedOnce: false,
          // },
          // removeXMLProcInst: false,
        },
      },
    },
    'convertStyleToAttrs',
    'removeDimensions',
  ],
};
