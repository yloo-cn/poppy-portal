## 入门指南

项目初始化:

```bash
npx create-next-app poppy-portal --use-pnpm --typescript --eslint -e with-tailwindcss
# 启用TS、ESLint
# 
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

项目运行:

```bash
cd poppy-portal
# 启动项目
pnpm dev
# 编译项目
pnpm run build
# 运行项目
pnpm start
```

打开首页 [http://localhost:3000](http://localhost:3000)

## 安装配置tailwindcss

```shell
# 安装tailwindcss以及其他依赖
pnpm add -D tailwindcss@latest postcss@latest autoprefixer@latest
# 生成 tailwind.config.js 和 postcss.config.js 文件
npx tailwindcss init -p
```

修改`tailwind.config.js`

```js
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [],
  theme: {
    extend: {},
  },
  plugins: [],
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
}
```

创建`styles/tailwind.css`

```css
@tailwind base;
@tailwind components;
@tailwind utilities;

```

`pages/_app.tsx`引入`styles/tailwind.css`

```tsx
import '../styles/globals.css'
import "../styles/tailwind.css";
import type { AppProps } from 'next/app'

export default function App({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}

```


## 安装配置antd

```shell
# 安装antd以及其他依赖
pnpm add classnames swiper dayjs next-i18next antd antd-mobile
# 国际化
pnpm add next-i18next react-i18next i18next
pnpm add -D sass
pnpm add -D svgo @svgr/webpack

pnpm add -D cross-env
pnpm add ahooks dayjs lodash-es js-cookie @types/js-cookie lowdb
pnpm add nodemailer

pnpm add -D babel-plugin-import stylelint stylelint-config-standard stylelint-config-prettier stylelint-config-standard-scss stylelint-config-prettier-scss
pnpm add -D stylelint stylelint-config-standard stylelint-config-rational-order stylelint-config-prettier stylelint-config-standard-scss stylelint-config-prettier-scss
# 生成 tailwind.config.js 和 postcss.config.js 文件
npx tailwindcss init -p
```
