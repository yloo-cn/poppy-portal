// @ts-check
/**
 * @type {import('next-i18next').UserConfig}
 */
module.exports = {
  i18n: {
    defaultLocale: 'zh',
    locales: ['zh', 'en'],
  },
  defaultNS: 'common',
  fallbackLng: 'zh',
  // localeDetection: false,
  // reloadOnPrerender: isLocalEnv,
  // saveMissing: true,
  // serializeConfig: false,
  // use: [i18nextMiddleware.LanguageDetector],
  // detection: {
  //   caches: ['cookie'],
  //   lookupCookie: 'i18next',
  //   order: ['cookie', 'path', 'header'],
  // },

  // https://www.i18next.com/overview/configuration-options#logging
  // debug: process.env.NODE_ENV === 'development',
  /** To avoid issues when deploying to some paas (vercel...) */
  // localePath:
  //   typeof window === 'undefined'
  //     ? require('path').resolve('./public/locales')
  //     : '/locales',

  reloadOnPrerender: process.env.NODE_ENV === 'development',

  /**
   * @link https://github.com/i18next/next-i18next#6-advanced-configuration
   */
  // saveMissing: false,
  // strictMode: true,
  // serializeConfig: false,
  // react: { useSuspense: false }
};
