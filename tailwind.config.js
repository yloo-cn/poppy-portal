module.exports = {
  content: [],
  theme: {
    // screens: {
    //   sm: { max: '767px' },
    //   md: { min: '768px', max: '1023px' },
    //   lg: { min: '1024px', max: '1279px' },
    //   xl: { min: '1280px', max: '1535px' },
    //   '2xl': { min: '1536px' },
    // },
    container: {
      center: true,
      width: '1280px',
      background: '#f90',
      padding: {
        DEFAULT: '1rem',
        '2xl': '2rem',
      },
    },
    fontSize: {
      xs: ['0.75rem'],
      sm: ['0.875rem'],
      base: ['1rem'],
      lg: ['1.125rem'],
      xl: ['1.25rem'],
      '2xl': ['1.5rem'],
      '3xl': ['1.875rem'],
      '4xl': ['2.25rem'],
      '5xl': ['3rem'],
      '6xl': ['3.75rem'],
      '7xl': ['4.5rem'],
      '8xl': ['6rem'],
      '9xl': ['8rem'],
    },
    extend: {},
  },
  important: true,
  corePlugins: {
    preflight: false,
    // lineHeight: false,
  },
  variants: {
    container: [],
  },
  plugins: [],
  purge: ['./src/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
};
