module.exports = {
  extends: [
    // 'stylelint-config-standard',
    'stylelint-config-rational-order',
    'stylelint-config-standard-scss',
    // 'stylelint-config-prettier-scss',
    'stylelint-config-prettier',
  ],
  // plugins: ['stylelint-order', 'stylelint-config-rational-order/plugin'],
  // customSyntax: 'postcss-scss',
  rules: {
    // 项目配置
    // 设置border属性属于box模型部分
    'plugin/rational-order': [true, { 'border-in-box-model': true }],
    // 颜色指定大写
    'color-hex-case': 'upper',
    // 禁止空块
    'block-no-empty': true,
    // 颜色6位长度
    'color-hex-length': 'short',
    // 兼容自定义标签名
    'selector-type-no-unknown': [
      true,
      {
        ignoreTypes: [],
      },
    ],
    // 忽略伪类选择器 ::v-deep
    'selector-pseudo-element-no-unknown': [
      true,
      {
        ignorePseudoElements: ['v-deep', 'input-placeholder'],
      },
    ],
    // 忽略伪元素选择器 :global
    'selector-pseudo-class-no-unknown': [
      true,
      {
        ignorePseudoClasses: ['global'],
      },
    ],
    // 禁止低优先级的选择器出现在高优先级的选择器之后。
    'no-descending-specificity': null,
    // 不验证@未知的名字，为了兼容scss的函数
    'at-rule-no-unknown': null,
    'scss/at-rule-no-unknown': [
      true,
      {
        ignoreAtRules: ['tailwind'],
      },
    ],
    'scss/dollar-variable-empty-line-before': null,
    'scss/comment-no-empty': null,
    // 禁止空注释
    'comment-no-empty': null,
    // 禁止简写属性的冗余值
    'shorthand-property-no-redundant-values': true,
    // 禁止值的浏览器引擎前缀
    'value-no-vendor-prefix': true,
    // property-no-vendor-prefix
    'property-no-vendor-prefix': null,
    // 禁用小于 1 的小数有一个前导零
    'number-leading-zero': 'never',
    // 禁止空第一行
    'no-empty-first-line': true,
    'value-keyword-case': null,
    // 'order/order': ['declarations', 'custom-properties', 'dollar-variables', 'rules', 'at-rules'],
    // 'no-empty-source': null,
    // 'property-no-vendor-prefix': [true, { ignoreProperties: ['background-clip'] }],
    // 'number-no-trailing-zeros': true,
    // 'length-zero-no-unit': true,
    // 'value-list-comma-space-after': 'always',
    // 'declaration-colon-space-after': 'always',
    // 'value-list-max-empty-lines': 0,
    // 'declaration-block-no-duplicate-properties': true,
    // 'declaration-block-no-redundant-longhand-properties': true,
    // 'declaration-block-semicolon-newline-after': 'always',
    // 'block-closing-brace-newline-after': 'always',
    // 'media-feature-colon-space-after': 'always',
    // 'media-feature-range-operator-space-after': 'always',
    // 'at-rule-name-space-after': 'always',
    // indentation: 2,
    // 'no-eol-whitespace': true,
    // 'string-no-newline': null,
    // 屏蔽没有申明通用字体
    'font-family-no-missing-generic-family-keyword': null,
    'font-family-name-quotes': 'always-unless-keyword',
    'function-name-case': ['lower', { ignoreFunctions: ['/colorPalette/'] }],
    'function-no-unknown': [
      true,
      {
        ignoreFunctions: [
          'extend',
          'extract',
          'fade',
          'fadeout',
          'tint',
          'darken',
          'ceil',
          'fadein',
          'floor',
          'unit',
          'shade',
          'lighten',
          'percentage',
          '-',
          '~`colorPalette',
        ],
      },
    ],
    'no-invalid-position-at-import-rule': null,
    'declaration-empty-line-before': null,
    'keyframes-name-pattern': null,
    'custom-property-pattern': null,
    'number-max-precision': 8,
    'alpha-value-notation': 'number',
    'color-function-notation': 'legacy',
    'selector-class-pattern': null,
    'selector-id-pattern': null,
    'selector-not-notation': null,
  },
};
