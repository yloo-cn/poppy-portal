import commonJSON from './public/locales/zh_cn/common.json';

declare module 'react-i18next' {
  interface CustomTypeOptions {
    resources: {
      common: typeof commonJSON;
    };
  }
}
