export const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL ? process.env.NEXT_PUBLIC_BASE_URL : '';

export const isBrowser = () => {
  return typeof window !== 'undefined';
};

export const isMobile = () => {
  if (!isBrowser()) {
    return false;
  }
  const ua = navigator.userAgent;
  return ua.indexOf('Android') > 0 || ua.indexOf('iPhone') > 0 || ua.indexOf('iPad') > 0;
};
