import { getAppConfig, getAppContext } from '@/common/config';
import axios, { AxiosRequestConfig } from 'axios';
import Router from 'next/router';

export class Auth {
  refreshTokenUrl: any;
  constructor(props: any = {}) {}
  refreshToken = async (config: AxiosRequestConfig) => {
    const appContext = getAppContext();
    const { refreshTokenUrl } = getAppConfig();
    const { getRefreshToken, setToken, setRefreshToken } = appContext;
    return await axios
      .post(
        refreshTokenUrl,
        {},
        {
          headers: {
            ...config.headers,
            Authorization: 'Bearer ' + getRefreshToken(),
          },
        },
      )
      .then((response) => {
        const { token, refreshToken } = response.data.data;
        setRefreshToken(refreshToken);
        setToken(token);

        config.headers!.Authorization = 'Bearer ' + token;

        return config;
      });
  };
}

let auth: Auth;

/** 获取Auth 实例对象 */
export const getAuth = () => {
  if (!auth) {
    auth = new Auth();
  }
  return auth;
};

/** 设置Auth 实例对象 */
export const setAuth = (instance: Auth) => {
  if (!instance) {
    auth = instance;
  }
};
