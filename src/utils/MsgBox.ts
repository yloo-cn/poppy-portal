import { message, Modal, notification } from 'antd';

/**
 * 适配消息框配置参数
 * @param {*} config 配置项
 */
const messageAdapter = (config: any) => {
  const option = {
    style: {
      // marginTop: '35vh',
    },
    ...config,
  };
  if (option.message) {
    option.content = option.message;
    delete option.message;
  }

  return option;
};
const modalAdapter = (config: any) => {
  const option = {
    centered: true,
    duration: 2,
    width: 500,
    bodyStyle: { maxHeight: '500px', overflowY: 'auto', wordWrap: 'break-word' },
    ...config,
  };
  return option;
};
const notificationAdapter = (config: any) => {
  const option = {
    duration: 2,
    style: { width: 390, maxHeight: '500px', overflowY: 'auto', wordWrap: 'break-word' },
    ...config,
  };
  if (option.title) {
    option.message = option.title;
    delete option.title;
  }
  if (option.content) {
    option.description = option.content;
    delete option.content;
  }
  return option;
};

const MsgBox = {
  confirm(config: any) {
    return Modal.confirm(modalAdapter({ autoFocusButton: 'cancel', ...config }));
  },

  error(config: { title?: string; content?: any; message?: string }) {
    return notification.error(notificationAdapter({ duration: 3, centered: true, ...config }));
  },

  open(config: any) {
    return notification.open(notificationAdapter(config));
  },
  info(config: any) {
    return message.info(messageAdapter(config));
  },
  success(config: { title: string; content: any }) {
    return message.success(messageAdapter(config));
  },
  warning(config: { message: any }) {
    return message.warning(messageAdapter({ duration: 3, ...config }));
  },
  getConfig(config: any) {
    return notificationAdapter(config);
  },
};

export default MsgBox;
