import axios, {
  Axios,
  AxiosDefaults,
  AxiosHeaders,
  AxiosInstance,
  AxiosInterceptorManager,
  AxiosRequestConfig,
  AxiosResponse,
  CancelTokenSource,
  CreateAxiosDefaults,
  HeadersDefaults,
} from 'axios';
import { i18n } from 'next-i18next';
import { getAppContext } from '@/common/config/AppContext';
// import MsgBox from './MsgBox';
import { message } from 'antd';
import { isBrowser } from './OsUtil';
import { getAuth } from './auth';
import { getLogDB } from '@/common/storage/LogDB';
import { getAppConfig } from '@/common/config';

export const BASE_URL = process.env.BASE_URL2 || '';

/** HTTP请求内容类型 */
export enum ContentType {
  JSON = 'application/json; charset=UTF-8',
  FORM = 'application/x-www-form-urlencoded; charset=UTF-8',
  FORM_DATA = 'multipart/form-data; charset=UTF-8',
  OBJECT = 'application/octet-stream',
}

/** Http method */
export enum HttpMethod {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  PATCH = 'PATCH',
  DELETE = 'DELETE',
}

// import { CODE_MESSAGE, ContentType } from '../constants';
// import type { MsgBoxProts } from '../types';

// import { getAppConfig } from './AppConfig';
// import { getAppContext } from './AppContext';

// let messageBox = (data: MsgBoxProts) => {
//   if (data.success) {
//     console.info({ title: '信息', content: data.message });
//   } else {
//     console.error({ title: '错误', content: data.message });
//   }
// };

// const setMsgBox = (msgBox: (data: MsgBoxProts) => void) => {
//   messageBox = msgBox;
// };

type Message = {
  error: (e: any) => void;
  warn: (e: any) => void;
  info: (e: any) => void;
  success: (e: any) => void;
};
let MsgBox: Message = {
  error: (e) => {
    message.error(e);
  },
  warn: (e: any) => {},
  info: (e: any) => {},
  success: (e: any) => {
    message.success(e);
  },
};

export const setMessage = (message: Message) => {
  MsgBox = { ...message };
};

class Request {
  instance: AxiosInstance;
  constructor(defaults: CreateAxiosDefaults = {}, option = {}) {
    const { baseURL = BASE_URL, timeout = getAppConfig().timeout, withCredentials = true, ...rest } = defaults;
    // 创建一个 axios 实例
    this.instance = axios.create({ withCredentials, baseURL, timeout, ...rest });
    // 设置默认值
    this.instance.defaults.headers.common['Cache-Control'] = 'no-cache';
    this.instance.defaults.headers.common['Content-Type'] = 'application/json';
    this.instance.defaults.headers.common['Accept-Language'] = 'zh-cn';

    // 添加拦截器
    this.addInterceptors();
  }

  /**
   * 添加拦截器
   */
  protected addInterceptors() {
    // 请求拦截器
    this.instance.interceptors.request.use(this.requestHandle, this.requestError);
    // 响应拦截器
    this.instance.interceptors.response.use(this.responseHandle, this.responseError);
  }

  /**
   * 请求配置拦截处理
   * @param config 请求配置
   * @returns 处理后的请求配置
   */
  protected requestHandle(config: AxiosRequestConfig) {
    // 在请求发送之前添加请求头信息
    Object.assign(config.headers || {}, {
      'Accept-Language': i18n?.language || 'zh',
    });
    // 浏览器端运行
    if (isBrowser()) {
      const appContext = getAppContext();
      const { getToken, getUser } = appContext;
      const token = getToken();
      Object.assign(config.headers || {}, {
        Authorization: token ? `Bearer ${token}` : null,
        token: token,
        userId: getUser()?.getUserId(),
        post: getUser()?.getPost(),
      });
    }

    return config;
  }

  /**
   * 请求错误处理
   * @param error 请求错误
   * @returns 请求错误
   */
  protected requestError(error: any) {
    console.error(error, '------requestError');
    // getDatabase().setLogData('error', '-------------------requestError');
    return Promise.reject(error);
  }

  /**
   * 请求错误处理
   * @param error 请求错误
   * @returns 请求错误
   */
  protected responseError(error: any) {
    const { message, response, config, request } = error;
    const msg = response?.data?.message || message;

    if (isBrowser()) {
      const db = getLogDB();
      db.setItem(null, {
        type: 'error',
        url: request.responseURL || config.url,
        message: msg,
        detail: JSON.stringify(response || { request, config }, null, 4),
      });
      MsgBox.error(msg.toString());
    }
    return Promise.reject(error);
  }

  /**
   * 响应信息处理
   * @param response 响应信息
   * @returns 响应信息
   */
  protected preHandle = (response: AxiosResponse<any, any>) => {
    const { headers } = response;

    if (headers['content-type'] === ContentType.OBJECT) {
      // 下载文件
      return response;
    }
    const { data } = response;
    if (!isBrowser()) {
      return data;
    }
    const token = headers.token;
    if (token) {
      getAppContext().setToken(token);
    }

    const { code, success, message } = data;
    if (code === 200 && success) {
      return data;
    }
    if (message && code !== 401 && code !== 403) {
      if (success) {
        MsgBox.success(message);
      } else {
        MsgBox.warn(message);
      }
      return data;
    }
    return null;
  };

  /**
   * 响应信息处理
   * @param response 响应信息
   * @returns 响应信息
   */
  protected responseHandle = async (response: AxiosResponse<any, any>) => {
    const result = this.preHandle(response);
    if (result) {
      return result;
    }
    const { data, config } = response;
    const { code, success, message } = data;
    if (code === 401) {
      return getAuth()
        .refreshToken(config)
        .then((newConfig) => {
          return axios(newConfig)
            .then((res) => {
              const result = this.preHandle(res);
              if (result !== null) {
                return result;
              }
              return res.data;
            })
            .catch((error) => {
              MsgBox.error(error || i18n?.t('operation.error'));
            });
        })
        .catch((error) => {
          getAppContext().logoutSession();
          MsgBox.error(error || i18n?.t('status_code.401'));
        });
    } else if (!success) {
      MsgBox.warn(message || i18n?.t('operation.warn'));
    }

    return data;
  };
}

const request = new Request().instance;
export default request;
