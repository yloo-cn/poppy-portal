import Footer from '@/components/Footer';
import Header from '@/components/Header';
import Online from '@/components/Online';
import { ReactElement } from 'react';

interface Props {
  children?: ReactElement;
}

const PortalLayout: React.FC<Props> = ({ children }) => {
  return (
    <>
      <Header />
      <main>{children}</main>
      <Footer />
      <Online />
    </>
  );
};

export default PortalLayout;

export function getPortalLayout(children: ReactElement) {
  return <PortalLayout>{children}</PortalLayout>;
}
