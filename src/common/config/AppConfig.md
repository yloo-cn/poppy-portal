---
title: AppConfig
nav:
  path: /common
group:
  path: /
  # title: AppConfig
---

## AppConfig 应用上下文


## API

| 属性名     | 描述       | 类型       | 默认值 |
| ---------- | ---------- | ---------- | ------ |
| appVersion | 版本       | `string`   | -      |
| keyPrefix  | key前缀    | `string`   | -      |
| loginUrl   | 登录地址   | `string`   | -      |
| homePage   | 首页Tab    | `string`   | -      |
| dbName     | 数据库名称 | `string`   | -      |
| dbVersion  | 数据库版本 | `string`   | -      |
| tables     | 数据库表   | `string[]` | -      |
