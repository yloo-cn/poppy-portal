import { Cookie, Storage } from '../storage';

import { getAppConfig } from './AppConfig';
import type { UserInfo } from '../../types/UserInfo';
import {
  K_DATA_SECURITY,
  K_EXPIRY_TIME,
  K_LOGIN_INFO,
  K_RIGHTS,
  K_SYSTEM_LOCK,
  K_TOKEN,
  K_USER,
  K_HOTKEYS,
  K_ONLY_USE_KEYBOARD,
  K_REFRESH_TOKEN,
} from '../constants/property';

interface LoginInfo extends UserInfo {
  token: string;
  expiryTime: number;
}

/**
 * 上下文工具类，用于存储和获取当前用户的会话值
 *
 * @author huangsq
 */
export default class AppContext {
  readonly cookie: Cookie;
  readonly local: Storage;
  readonly session: Storage;
  readonly loginUrl: string;

  constructor() {
    const { loginUrl, keyPrefix } = getAppConfig();
    this.loginUrl = loginUrl;
    this.cookie = new Cookie('');
    this.local = new Storage(window.localStorage, keyPrefix);
    this.session = new Storage(window.sessionStorage, keyPrefix);
  }

  getExpiryTime = (): number => {
    return this.local.getItem(K_EXPIRY_TIME) || 0;
  };

  setExpiryTime = (expiryTime: number | null) => {
    return this.local.setItem(K_EXPIRY_TIME, expiryTime);
  };

  isSystemLock = (): boolean => {
    return this.local.getItem(K_SYSTEM_LOCK);
  };

  setSystemLock = (systemLock: boolean | null) => {
    return this.local.setItem(K_SYSTEM_LOCK, systemLock);
  };

  getToken = () => {
    // 优先从cookie中获取token
    let token = this.cookie.getItem(K_TOKEN);
    if (!token) {
      token = this.local.getItem(K_TOKEN);
      if (token) {
        this.cookie.setItem(K_TOKEN, token);
      }
    }
    return token;
  };

  setToken = (token: string | null) => {
    if (token) {
      this.cookie.setItem(K_TOKEN, token);
      // 自动登录存储Token到local中
      if (this.isAutoLogin()) {
        this.local.setItem(K_TOKEN, token);
      }
    } else {
      this.local.remove(K_TOKEN);
      this.cookie.remove(K_TOKEN);
    }
  };
  getRefreshToken = () => {
    // 优先从cookie中获取token
    let token = this.cookie.getItem(K_REFRESH_TOKEN);
    if (!token) {
      token = this.local.getItem(K_REFRESH_TOKEN);
      if (token) {
        this.cookie.setItem(K_REFRESH_TOKEN, token);
      }
    }
    return token;
  };
  setRefreshToken = (token: string | null) => {
    if (token) {
      this.cookie.setItem(K_REFRESH_TOKEN, token);
      // 自动登录存储Token到local中
      if (this.isAutoLogin()) {
        this.local.setItem(K_REFRESH_TOKEN, token);
      }
    } else {
      this.local.remove(K_REFRESH_TOKEN);
      this.cookie.remove(K_REFRESH_TOKEN);
    }
  };

  getLoginInfo = () => {
    return this.local.getObject(K_LOGIN_INFO);
  };
  setLoginInfo = (user: any) => {
    this.local.setObject(K_LOGIN_INFO, user);
  };
  getUser = () => {
    return this.session.getObject(K_USER);
  };
  setUser = (user: any) => {
    this.session.setObject(K_USER, user);
  };
  getUserId = () => {
    return this.getUser()?.id || null;
  };
  getPost = () => {
    return this.getUser()?.post || null;
  };
  getRights = () => {
    return this.session.getObject(K_RIGHTS);
  };
  setRights = (rights: any) => {
    return this.session.setObject(K_RIGHTS, rights);
  };
  getDataSecurity = () => {
    return this.session.getObject(K_DATA_SECURITY);
  };
  setDataSecurity = (dataSecurity: any) => {
    return this.session.setObject(K_DATA_SECURITY, dataSecurity);
  };

  isAutoLogin = () => {
    const { autoLogin = false } = this.getLoginInfo() || {};
    return autoLogin;
  };

  isExpiry = () => {
    return new Date().getTime() > this.getExpiryTime();
  };

  isLogin = () => {
    const token = this.getToken();
    return token && token?.length > 9;
  };

  /** 设置全局快捷键 */
  setHotkeys(hotkeys: any) {
    return this.local.setObject(K_HOTKEYS, hotkeys);
  }
  /** 获取快捷键配置 */
  getHotkeys() {
    return this.local.getObject(K_HOTKEYS);
  }
  /** 设置密码强制启用软键盘 */
  setOnlyUseKeyboard(onlyUseKeyboard: boolean) {
    return this.local.setObject(K_ONLY_USE_KEYBOARD, onlyUseKeyboard);
  }
  /** 获取密码强制启用软键盘状态 */
  getOnlyUseKeyboard() {
    return this.local.getObject(K_ONLY_USE_KEYBOARD);
  }

  loginSession = (loginInfo: Partial<LoginInfo>) => {
    const { token = null, expiryTime = 0, ...userInfo } = loginInfo;
    this.setLoginInfo(userInfo);
    this.setToken(token);
    this.setExpiryTime(expiryTime);
  };

  logoutSession = () => {
    this.setToken(null);
    this.setExpiryTime(null);
    this.setLoginInfo(null);
    this.setUser(null);
    this.setRights(null);
    this.setDataSecurity(null);
    location.replace(this.loginUrl);
  };
}

let appContext: AppContext;
/** 获取AppContext 实例对象 */
export const getAppContext = () => {
  if (!appContext) {
    appContext = new AppContext();
  }
  return appContext;
};
