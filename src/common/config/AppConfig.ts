import { DB_NAME } from '../constants/table';

/**
 * 系统配置类
 *
 * @author huangsq
 */
export default class AppConfig {
  [x: string]: any;
  /** 版本 */
  appVersion: string;
  /** key前缀 */
  keyPrefix: string;
  timeout: number;
  dbName: string;
  dbVersion: number;
  tables: Record<string, object>;
  refreshTokenUrl: string;
  loginUrl: string;
  homePage: string;
  releaseLogRatio: number;

  constructor() {
    this.appVersion = '1.0.0';
    this.keyPrefix = 'poppy-1.0.0-';
    this.timeout = 30000;
    this.dbName = DB_NAME;
    this.dbVersion = 1;
    this.tables = {};
    this.loginUrl = '/login';
    this.refreshTokenUrl = '/api/login/refresh';
    this.homePage = '/welcome';
    this.maxLog = 20;
    this.releaseLogRatio = 25;
  }

  /**
   * 设置配置文件，合并配置项
   * @param config 配置信息
   */
  setConfig = (config: Record<string, any>) => {
    Object.entries(config).forEach(([key, value]) => {
      if (value !== null) {
        this[key] = value;
      }
    });
  };
}

let appConfig: AppConfig;
/**
 * 获取AppConfig 实例对象
 */
export const getAppConfig = () => {
  if (!appConfig) {
    appConfig = new AppConfig();
  }
  return appConfig;
};
