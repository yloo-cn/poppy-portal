---
title: AppContext
nav:
  path: /common
group:
  path: /
  # title: AppContext
---

## AppContext 应用上下文


## API

| 属性名          | 描述         | 类型        | 默认值 |
| --------------- | ------------ | ----------- | ------ |
| expiryTime      | 过期时间     | `number`    | -      |
| systemLock      | 是否锁定系统 | `boolean`   | -      |
| token           | 登录token    | `string`    | -      |
| loginInfo       | 登录信息     | `LoginInfo` | -      |
| user            | 用户信息     | `User`      | -      |
| userId          | 用户ID       | `string`    | -      |
| post            | 岗位信息     | `[]`        | -      |
| rights          | 权限信息     | `[]`        | -      |
| dataSecurity    | 数据安全     | `{}`        | -      |
| autoLogin       | 自动登录     | `boolean`   | -      |
| hotkeys         | 热键         | `{}`        | -      |
| onlyUseKeyboard | 开启软键盘   | `boolean`   | -      |


| 方法名        | 描述           | 类型         | 默认值 |
| ------------- | -------------- | ------------ | ------ |
| getAppContext | 获取上下文配置 | `AppContext` | -      |
| isExpiry      | 是否过期       | `boolean`    | -      |
| isLogin       | 是否登录       | `boolean`    | -      |
| loginSession  | 登录会话       | `string`     | -      |
| logoutSession | 注销会话       | `string`     | -      |

