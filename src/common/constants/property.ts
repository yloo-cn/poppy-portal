/** 过期时间 */
export const K_EXPIRY_TIME = 'expiryTime';
/** Token */
export const K_TOKEN = 'token';
/** Token */
export const K_REFRESH_TOKEN = 'refreshToken';
/** 登录用户 */
export const K_USER = 'user';
/** 用户权限 */
export const K_RIGHTS = 'rights';
/** 数据安全 */
export const K_DATA_SECURITY = 'dataSecurity';
/** 系统锁定 */
export const K_SYSTEM_LOCK = 'systemLock';
/** 登录信息 */
export const K_LOGIN_INFO = 'loginInfo';
/** 配置信息 */
export const K_CONFIG = 'config';
/** 自动登录 */
export const K_AUTO_LOGIN = 'autoLogin';
/** 强制启用软键盘 */
export const K_ONLY_USE_KEYBOARD = 'onlyUseKeyboard';
// 快捷键
export const K_HOTKEYS = 'hotkeys';
