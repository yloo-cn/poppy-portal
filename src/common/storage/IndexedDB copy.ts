export type DBTable = {
  dbName: string;
  storeOptions: Record<string, any>;
  version?: number;
};

/**
 * Web缓存类，用于封装存储和获取对象值
 *
 * @author huangsq
 */
export default class IndexedDB {
  dbName: string;
  storeOptions: Record<string, any>;
  version: number;
  storage: any;
  db: Promise<IDBDatabase>;

  /**
   * 删除指定数据库
   *
   * @param dbName 数据库名称
   */
  static deleteDB(dbName: string) {
    window?.indexedDB?.deleteDatabase(dbName);
  }

  constructor(options: DBTable) {
    const { dbName, storeOptions, version = 1 } = options;
    this.dbName = dbName;
    this.storeOptions = storeOptions;
    this.version = version;
    this.db = this.openDB({ dbName, storeOptions, version });
  }

  /**
   * 根据主键删除指定表和主键中数据
   *
   * @param table 表名
   * @param key 主键
   * @returns Request
   */
  deleteItem(table: string, key: string) {
    const request = this.storage.transaction(table, 'readwrite').objectStore(table).delete(key);
    return new Promise<any>((resolve, reject) => {
      request.onsuccess = (event: any) => {
        resolve(event);
      };
      request.onerror = (event: any) => {
        reject(event);
      };
    });
  }

  /**
   * 获取当前数据库下指定表和主键对应的值,通过回调函数返回数据
   *
   * @param table 表名
   * @param key 主键，key为null返回所有记录
   * @returns Promise
   */
  getItem = <T>(table: string, key: string | null) => {
    // 第二个参数可以省略
    const transaction = this.storage.transaction(table, 'readwrite');
    const store = transaction.objectStore(table);
    let request: any = null;
    if (key) {
      request = store.get(key);
    } else {
      request = store.getAll();
    }

    return new Promise<T>((resolve, reject) => {
      request.onsuccess = (event: { target: { result: any } }) => {
        resolve(event?.target?.result);
      };
      request.onerror = (event: any) => {
        reject(event);
      };
    });
  };

  /**
   * 新增或更新数据到指定的表和主键中
   *
   * @param table 表名
   * @param key 主键
   * @param value 值
   * @returns Request
   */
  setItem = <T>(table: string, key: string, value: T) => {
    const transaction = this.storage.transaction(table, 'readwrite');
    const store = transaction.objectStore(table);
    const request = store.put(value, key);

    return new Promise<T>((resolve, reject) => {
      request.onsuccess = (event: any) => {
        resolve(event);
      };
      request.onerror = (event: any) => {
        reject(event);
      };
    });
  };

  /** 清空表数据 */
  clear(table: string) {
    const request = this.storage.transaction(this.dbName, 'readwrite').objectStore(table).clear();

    return new Promise((resolve, reject) => {
      request.onsuccess = (event: any) => {
        resolve(event);
      };
      request.onerror = (event: any) => {
        reject(event);
      };
    });
  }

  /** 获取数据库对象 */
  getDB(): any {
    return this.storage;
  }

  /** 关闭数据库连接 */
  closeDB() {
    this.storage.close();
  }

  /**
   * 打开数据库
   *
   * @param options
   * @returns
   */
  openDB({ dbName, version, storeOptions }: DBTable) {
    const { indexedDB } = window;

    return new Promise<IDBDatabase>((resolve, reject) => {
      // 获取缓存的数据库
      if (this.storage) {
        resolve(this.storage);
        return;
      }
      if (!indexedDB) {
        reject('indexedDB is null');
        return;
      }
      const request = indexedDB.open(dbName, version);
      // 创建新的store
      request.onupgradeneeded = (event: any) => {
        const database = event.target.result;
        for (const key in storeOptions) {
          if (!database.objectStoreNames.contains(key)) {
            const keyPath = storeOptions[key] ? storeOptions[key] : [];
            database.createObjectStore(key, { keyPath });
          }
        }
        this.storage = database;
        resolve(database);
      };
      // 数据库连接成功
      request.onsuccess = (event: any) => {
        this.storage = event.target.result;

        resolve(this.storage);
      };
      request.onerror = (event) => {
        reject(event);
      };
    });
  }
}
