import { getAppConfig } from '../config';
import { T_CONFIG, T_DICT, T_LOG } from '../constants/table';
import IndexedDB from './IndexedDB';

export const defaultTables = {
  [T_CONFIG]: {
    autoIncrement: true,
    keyPath: 'id',
  },
  [T_DICT]: {
    autoIncrement: true,
    keyPath: 'id',
  },
  [T_LOG]: {
    autoIncrement: true,
    keyPath: 'id',
  },
};

let database: IndexedDB;
/** 获取IndexedDB 实例对象 */
const getDatabase = () => {
  if (!database) {
    const { dbName, tables } = getAppConfig();
    database = new IndexedDB({ dbName, storeOptions: { ...defaultTables, ...tables } });
  }
  return database;
};
export default getDatabase;
