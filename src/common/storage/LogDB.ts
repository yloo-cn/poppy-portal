import { LogInfo } from '@/types';
import { getAppConfig } from '../config';
import { T_LOG } from '../constants/table';
import getDatabase from './DataBase';
import IndexedDB from './IndexedDB';

/**
 * 前端数据库日志表
 *
 * @author huangsq
 */
export class LogDB {
  db: IndexedDB;
  table: string;
  maxLog: any;
  releaseLogRatio: any;

  constructor(table = T_LOG) {
    this.table = table;
    const { maxLog, releaseLogRatio } = getAppConfig();
    this.maxLog = maxLog;
    this.releaseLogRatio = releaseLogRatio / 100;
    this.db = getDatabase();
  }

  getItem = async (key: string) => {
    return await this.db.getItem(this.table, key);
  };

  getItems = async () => {
    return await this.db.getItems<LogInfo>(this.table);
  };

  setItem = async (key: string | null, logInfo: LogInfo) => {
    const keys = await this.db.getAllKeys(this.table);
    if (keys?.length > this.maxLog) {
      await this.releaseLogs();
    }
    return this.db.setItem(this.table, key, { ...logInfo, createTime: new Date().getTime() });
  };

  setItems = async (key: string | null, logInfo: LogInfo[]) => {
    const keys = await this.db.getAllKeys(this.table);
    if (keys?.length > this.maxLog) {
      await this.releaseLogs();
    }
    for (let i = 0; i < logInfo.length; i += 1) {
      if (typeof logInfo[i] === 'object') {
        return this.db.setItem(this.table, key, logInfo[i]);
      }
    }
  };

  deleteItem = async (table: string, key: string | Array<string> | IDBKeyRange) => {
    if (key instanceof Array) {
      for (let i = 0; i < key.length; i += 1) {
        await this.db.deleteItem(table, key[i]);
      }
      return;
    }
    return await this.db.deleteItem(table, key);
  };

  clear = async () => {
    this.db.clear(this.table);
  };

  releaseLogs = async () => {
    const keys = await this.db.getAllKeys(this.table);
    if (!(keys instanceof Array) || keys.length === 0) {
      return;
    }
    // 每次释放的日志
    const halfKey = keys[Math.floor(keys.length * this.releaseLogRatio)];
    console.log(IDBKeyRange.upperBound(halfKey), 'IDBKeyRange.upperBound(halfKey)');
    return await this.deleteItem(this.table, IDBKeyRange.upperBound(halfKey));
  };

  exports = async () => {
    const list = await this.getItems();
    const listJson = JSON.stringify(list, null, 2);
    const blob = new Blob([listJson], { type: 'text/json' });
    const a = document.createElement('a');
    a.href = window.URL.createObjectURL(blob);
    a.download = `前端日志_${new Date().getTime()}.json`;
    a.click();
  };

  imports = async (file: File) => {
    return new Promise((resolve) => {
      if (file.type !== 'application/json' || !window.FileReader) {
        return;
      }
      const fileRead = new FileReader();
      fileRead.onload = (event) => {
        const resultJSON = event.target?.result;
        if (resultJSON && typeof resultJSON === 'string') {
          const result = JSON.parse(resultJSON);
          if (result instanceof Array) {
            resolve(this.setItems(null, result));
          }
        }
      };
      fileRead.readAsText(file);
    });
  };
}

let logDB: LogDB;
/** 获取LogDB 实例对象 */
export const getLogDB = () => {
  if (!logDB) {
    logDB = new LogDB();
  }
  return logDB;
};
