import Cookies from 'js-cookie';

/**
 * Web缓存类，用于封装存储和获取对象值
 *
 * @author huangsq
 */
export default class Cookie {
  keyPrefix: string;

  constructor(keyPrefix: string) {
    this.keyPrefix = keyPrefix;
  }

  getKey(key: string) {
    return this.keyPrefix + key;
  }

  /**
   * 获取 cookie 值
   *
   * @param {string} name Cookie name
   */
  getItem(name: string) {
    return Cookies.get(this.getKey(name));
  }

  /**
   * 存储 cookie 值
   *
   * @param {string} name Cookie name
   * @param {string} value Cookie value
   * @param {Object} setting Cookie setting
   */
  setItem(name: string, value: string, options: any = {}) {
    const cookieSetting = {
      path: '/',
    };
    Object.assign(cookieSetting, options);
    Cookies.set(this.getKey(name), value, cookieSetting);
  }

  /**
   * 获取Json对象值
   *
   * @param {any} name
   */
  getObject(name: string) {
    const value = Cookies.get(this.getKey(name));
    return value ? JSON.parse(value) : value;
  }

  /**
   * 设置Json对象值
   *
   * @param {any} name
   * @param {any} value
   */
  setObject(name: string, value: any, options: any = {}) {
    this.setItem(name, value && JSON.stringify(value), options);
  }

  /**
   * 删除 cookie
   *
   * @param {string} name Cookie name
   */
  remove(name: string) {
    return Cookies.remove(this.getKey(name));
  }

  /** 拿到 cookie 全部的值 */
  getAll() {
    return Cookies.get();
  }
}
