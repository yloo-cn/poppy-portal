import { T_CONFIG, T_DICT } from '../constants/table';
import getDatabase from './DataBase';
import IndexedDB from './IndexedDB';

/**
 * 前端数据库缓存, 默认支持配置、字典表
 *
 * @author huangsq
 */
export default class DataCache {
  db: IndexedDB;

  constructor() {
    this.db = getDatabase();
  }

  getDictData = async (key: string) => {
    return await this.db.getItem(T_DICT, key);
  };

  setDictData = async (key: string, value: any) => {
    return await this.db.setItem(T_DICT, key, value);
  };

  removeDictData = async (key: string) => {
    return await this.db.deleteItem(T_DICT, key);
  };

  getConfig = async (key: string) => {
    return await this.db.getItem(T_CONFIG, key);
  };

  setConfig = async (key: string, value: any) => {
    return await this.db.setItem(T_CONFIG, key, value);
  };

  removeConfig = async (key: string) => {
    return await this.db.deleteItem(T_CONFIG, key);
  };

  getValue = async (table: string, key: string) => {
    return await this.db.getItem(table, key);
  };

  setValue = async (table: string, key: string, value: any) => {
    return await this.db.setItem(table, key, value);
  };

  removeValue = async (table: string, key: string) => {
    return await this.db.deleteItem(table, key);
  };
}

let dataCache: DataCache;
/** 获取DataCache 实例对象 */
export const getDataCache = () => {
  if (!dataCache) {
    dataCache = new DataCache();
  }
  return dataCache;
};
