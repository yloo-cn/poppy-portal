/**
 * Web存储类，用于封装存储和获取对象值
 *
 * @author huangsq
 */
export default class Storage {
  storage: any;

  keyPrefix: string;

  cache: Record<string, any>;

  constructor(storage: any, keyPrefix?: string) {
    this.storage = storage;
    this.keyPrefix = keyPrefix || '';
    this.cache = {};
  }

  getKey(key: string) {
    return this.keyPrefix + key;
  }

  removeCache(key: string) {
    return delete this.cache[key];
  }

  /**
   * 根据key获取存储值，key不带前缀
   *
   * @param {any} key
   */
  get(key: string) {
    return this.storage.getItem(key);
  }

  /**
   * 根据key设置存储值，key不带前缀
   *
   * @param {any} key
   * @param {any} value
   */
  set(key: string, value: any) {
    if (value === null) {
      this.storage.removeItem(key);
      return;
    }
    this.storage.setItem(key, value);
  }

  getItem(key: string) {
    return this.get(this.getKey(key));
  }

  setItem(key: string, value: any) {
    this.set(this.getKey(key), value);
  }

  /**
   * 获取Json对象值
   *
   * @param {any} key
   */
  getObject(key: string) {
    let value = this.cache[key];
    if (!value) {
      value = this.getItem(key);
      if (value) {
        value = JSON.parse(value);
      }
      this.cache[key] = value;
    }
    return value;
  }

  /**
   * 设置Json对象值
   *
   * @param {any} key
   * @param {any} value
   */
  setObject(key: string, value: any) {
    this.removeCache(key);
    this.setItem(key, value && JSON.stringify(value));
  }

  /**
   * 移除会话存储项
   *
   * @param {any} key
   */
  remove(key: string) {
    this.removeCache(key);
    this.storage.removeItem(this.getKey(key));
  }

  /** 清空会话存储项 */
  clear() {
    this.storage.clear();
  }

  /** 清空会话存储项 */
  getStorage(): any {
    return this.storage;
  }

  /** 获取会话存储项所有的key，不包含前缀 */
  keys() {
    const allKeys = [];
    for (let i = 0; i < this.storage.length; i += 1) {
      const key = this.storage.key(i)?.replace(this.keyPrefix, '');
      allKeys.push(key);
    }
    return allKeys;
  }

  /** 获取会话存储项条数 */
  length() {
    return this.storage.length;
  }
}
