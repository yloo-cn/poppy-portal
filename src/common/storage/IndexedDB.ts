export type DBTable = {
  dbName: string;
  storeOptions: Record<string, any>;
  version?: number;
};

/**
 * Web缓存类，用于封装存储和获取对象值
 *
 * @author huangsq
 */
export default class IndexedDB {
  dbName: string;
  storeOptions: Record<string, any>;
  version: number;
  storage: any;
  db: Promise<IDBDatabase>;

  /**
   * 删除指定数据库
   *
   * @param dbName 数据库名称
   */
  static deleteDB(dbName: string) {
    window?.indexedDB?.deleteDatabase(dbName);
  }

  constructor(options: DBTable) {
    const { dbName, storeOptions, version = 2 } = options;
    this.dbName = dbName;
    this.storeOptions = storeOptions;
    this.version = version;
    this.db = this.openDB({ dbName, storeOptions, version });
  }

  /**
   * 根据主键删除指定表和主键中数据
   *
   * @param table 表名
   * @param key 主键
   * @returns Request
   */
  deleteItem<T = any>(table: string, key: string | IDBKeyRange) {
    return this.db.then((db) => {
      return new Promise<T>((resolve, reject) => {
        const request = db.transaction(table, 'readwrite').objectStore(table).delete(key);
        request.onsuccess = (event: any) => {
          resolve(event);
        };
        request.onerror = (event: any) => {
          reject(event);
        };
      });
    });
  }

  /**
   * 获取表内所有的主键值
   * @param table 表名
   * @returns request
   */
  getAllKeys<T = any>(table: string) {
    return this.db.then((db) => {
      return new Promise<T>((resolve, reject) => {
        const request = db.transaction(table, 'readwrite').objectStore(table).getAllKeys();
        request.onsuccess = (event: any) => {
          resolve(event?.target?.result);
        };
        request.onerror = (event: any) => {
          reject(event);
        };
      });
    });
  }

  /**
   * 获取当前数据库下指定表和主键对应的值,通过回调函数返回数据
   *
   * @param table 表名
   * @param key 主键，key为null返回所有记录
   * @returns Promise
   */
  getItem = <T = any>(table: string, key: string) => {
    // 第二个参数可以省略
    return this.db.then((db) => {
      return new Promise<T>((resolve, reject) => {
        const store = db.transaction(table, 'readwrite').objectStore(table);
        let request = store.get(key);
        request.onsuccess = (event: any) => {
          resolve(event.target?.result);
        };
        request.onerror = (event: any) => {
          reject(event);
        };
      });
    });
  };

  /**
   * 获取当前数据库下指定表和主键对应的值,通过回调函数返回数据
   *
   * @param table 表名
   * @param key 主键，key为null返回所有记录
   * @returns Promise
   */
  getItems = <T = any>(table: string, key?: string | IDBKeyRange) => {
    // 第二个参数可以省略
    return this.db.then((db) => {
      return new Promise<Array<T>>((resolve, reject) => {
        const store = db.transaction(table, 'readwrite').objectStore(table);
        let request: any = null;
        if (key) {
          request = store.get(key);
        } else {
          request = store.getAll();
        }
        request.onsuccess = (event: any) => {
          resolve(event.target?.result);
        };
        request.onerror = (event: any) => {
          reject(event);
        };
      });
    });
  };

  /**
   * 新增或更新数据到指定的表和主键中
   *
   * @param table 表名
   * @param key 主键
   * @param value 值
   * @returns Request
   */
  setItem = <T = any>(table: string, key: string | null, value: T) => {
    return this.db.then((db) => {
      return new Promise<T>((resolve, reject) => {
        // const transaction = db.transaction(table, 'readwrite');
        const store = db.transaction(table, 'readwrite').objectStore(table);
        // transaction.oncomplete = function (event) {
        //   const store = db.transaction(table, 'readwrite').objectStore(table);
        //   var customerObjectStore = db.transaction("customers", "readwrite")
        // .objectStore("customers")
        //   .add({ id: "555-55-5555", name: "Donna", age: 32, email: "donna@home.org" });
        const request = key === null ? store.add(value) : store.put(value, key);
        request.onsuccess = (event: any) => {
          resolve(event);
        };
        request.onerror = (event: any) => {
          reject(event);
        };
        // };
      });
    });
  };

  /** 清空表数据 */
  clear<T = any>(table: string) {
    return this.db.then((db) => {
      const request = db.transaction(this.dbName, 'readwrite').objectStore(table).clear();
      return new Promise<T>((resolve, reject) => {
        request.onsuccess = (event: any) => {
          resolve(event);
        };
        request.onerror = (event: any) => {
          reject(event);
        };
      });
    });
  }

  /** 获取数据库对象 */
  getDB() {
    return this.db;
  }

  /** 关闭数据库连接 */
  closeDB() {
    return this.db.then((db) => {
      db.close();
    });
  }

  /**
   * 打开数据库
   *
   * @param options
   * @returns
   */
  openDB({ dbName, version, storeOptions }: DBTable) {
    const { indexedDB } = window;

    return new Promise<IDBDatabase>((resolve, reject) => {
      // 获取缓存的数据库
      if (this.storage) {
        resolve(this.storage);
        return;
      }
      if (!indexedDB) {
        reject('indexedDB is null');
        return;
      }
      const request = indexedDB.open(dbName, version);
      // 创建新的store
      request.onupgradeneeded = (event: any) => {
        const database = event.target.result;
        for (const key in storeOptions) {
          if (!database.objectStoreNames.contains(key)) {
            const option = storeOptions[key] ? storeOptions[key] : {};
            database.createObjectStore(key, option);
          }
        }
        console.log('onupgradeneeded');

        // this.storage = database;
        // resolve(database);
      };
      // 数据库连接成功
      request.onsuccess = (event: any) => {
        this.storage = event.target.result;
        console.log('onsuccess');
        resolve(this.storage);
      };
      request.onerror = (event) => {
        reject(event);
      };
    });
  }
}
