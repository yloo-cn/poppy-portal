---
title: Storage 存储
order: 1
nav:
  path: /common
  title: 工具
  order: 5
group:
  path: /utils
  title: 工具类
  order: 1
---

## Storage

前端存储方案，提供`Cookie` `localStorage` `sessionStorage` `IndexedDB`等存储方式，结合`core`项目，提供项目的整体存储前缀及相关的缓存策略。

<API src="./Storage.ts" />

## Cookie

<API src="./Cookie.ts" />

## IndexedDB

<API src="./IndexedDB.ts" />
