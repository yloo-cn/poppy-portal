export interface UserInfo {
  id: string;
  username: string;
  password?: string;
  nickname: string;
  loginType: string;
  autoLogin: boolean;
  avatar?: string;
}
