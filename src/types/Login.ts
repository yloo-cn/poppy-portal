export interface LoginParams {
  username: string;
  password: string;
  autoLogin: boolean;
  loginType: string;
  captcha?: string;
}

export interface LoginResult {
  token: string;
  expiryTime: number;
  status?: string;
  type?: string;
  currentAuthority?: string;
}

export interface Captcha {
  captcha?: string;
  image?: string;
}

export type FakeCaptcha = {
  code?: number;
  status?: string;
};
