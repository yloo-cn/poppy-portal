/** 返回的状态码 */
export enum StatusCode {
  S200 = 200, // 成功
  S201 = 201,
  S202 = 202,
  S204 = 204,
  S400 = 400,
  S401 = 401,
  S403 = 403,
  S404 = 404,
  S405 = 405,
  S406 = 406,
  S408 = 408,
  S410 = 410,
  S422 = 422,
  S500 = 500,
  S501 = 501,
  S502 = 502,
  S503 = 503,
  S504 = 504,
  S505 = 505,
}

/** 状态码信息 */
export const CODE_MESSAGE = {
  [StatusCode.S200]: '操作成功',
  [StatusCode.S201]: '新建或修改数据成功',
  [StatusCode.S202]: '服务器已接受请求，但尚未处理',
  [StatusCode.S204]: '删除数据成功',
  [StatusCode.S400]: '请求参数错误',
  [StatusCode.S401]: '用户没有权限（令牌、用户名、密码错误）',
  [StatusCode.S403]: '很抱歉，您的访问请求被禁止！',
  [StatusCode.S404]: '很抱歉，您访问的资源不存在！',
  [StatusCode.S405]: '请求资源不存在或访问被禁止！',
  [StatusCode.S406]: '请求的资源的内容特性无法满足请求头中的条件',
  [StatusCode.S408]: '请求超时',
  [StatusCode.S410]: '请求的资源被永久删除，且不会再得到的',
  [StatusCode.S422]: '当创建一个对象时，发生一个验证错误',
  [StatusCode.S500]: '服务器发生错误，请检查服务器',
  [StatusCode.S501]: '服务未实现',
  [StatusCode.S502]: '网络连接错误',
  [StatusCode.S503]: '服务不可用，服务器暂时过载或维护',
  [StatusCode.S504]: '网关连接超时',
  [StatusCode.S505]: 'HTTP版本不受支持',
};
