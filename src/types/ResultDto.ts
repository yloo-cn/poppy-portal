import { StatusCode } from './StatusCode';

/** 基础返回数据 */
export interface ResultDto<T> {
  /** 状态码 */
  code: StatusCode | number;
  /** 操作信息 */
  message: string;
  /** 操作成功 */
  success: boolean;
  /** 更新时间 */
  updateTime?: string;
  /** 返回记录 */
  data?: T;
}

/** 列表数据 */
export interface RecordsDto<T> {
  /** 记录及列表 */
  records: T[];
  /** 总条数 */
  total: number;
}
