export interface LogInfo {
  id?: string;
  type: 'warn' | 'info' | 'error' | 'success';
  url?: string;
  href?: string;
  message: string;
  createTime?: number;
  detail?: string;
}
