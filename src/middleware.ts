import { withAuth } from 'next-auth/middleware';

// More on how NextAuth.js middleware works: https://next-auth.js.org/configuration/nextjs#middleware
export default withAuth({
  callbacks: {
    authorized(props) {
      const { req, token } = props;
      // `/admin` requires admin role
      if (req.nextUrl.pathname === '/admin') {
        console.log('+_________________________', req);
        return token?.userRole === 'admin';
      }
      // `/me` only requires the user to be logged in
      return !!token;
    },
  },
});

export const config = { matcher: ['/admin', '/me'] };
