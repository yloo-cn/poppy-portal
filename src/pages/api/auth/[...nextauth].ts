import NextAuth, { Awaitable, NextAuthOptions, RequestInternal, User } from 'next-auth';
// import CredentialsProvider from 'next-auth/providers/credentials';
import GithubProvider from 'next-auth/providers/github';
import Auth0Provider from 'next-auth/providers/auth0';
// import AppleProvider from "next-auth/providers/apple"
// import EmailProvider from 'next-auth/providers/email';

// For more information on each option (and a full list of options) go to
// https://next-auth.js.org/configuration/options
export const authOptions: NextAuthOptions = {
  // https://next-auth.js.org/configuration/providers/oauth
  providers: [
    // EmailProvider({
    //   name: 'qq-circle',
    //   server: process.env.EMAIL_SERVER,
    //   from: process.env.EMAIL_FROM,
    // }),
    // Temporarily removing the Apple provider from the demo site as the
    // callback URL for it needs updating due to Vercel changing domains

    // Providers.Apple({
    //   clientId: process.env.APPLE_ID,
    //   clientSecret: {
    //     appleId: process.env.APPLE_ID,
    //     teamId: process.env.APPLE_TEAM_ID,
    //     privateKey: process.env.APPLE_PRIVATE_KEY,
    //     keyId: process.env.APPLE_KEY_ID,
    //   },
    // }),
    // CredentialsProvider({
    //   name: 'wechat',
    //   credentials: {},
    //   authorize: function (credentials: any, req: Pick<RequestInternal, 'body' | 'query' | 'headers' | 'method'>) {
    //     // async authorize(credentials, req) {
    //     //   //具体授权逻辑
    //     //   const user = await getUser(credentials.userName);
    //     //   if (user?.password === credentials.password) {
    //     //     return { name: user.userName };
    //     //   }
    //     //   return { status: 'reject' };
    //     // },
    //     return {};
    //   },
    // }),
    Auth0Provider<any>({
      name: 'alipay-circle',
      clientId: process.env.AUTH0_ID,
      clientSecret: process.env.AUTH0_SECRET,
      issuer: process.env.AUTH0_ISSUER,
    }),
    GithubProvider({
      name: 'github',
      clientId: process.env.GITHUB_ID,
      clientSecret: process.env.GITHUB_SECRET,
    }),
  ],
  theme: {
    colorScheme: 'light',
  },
  callbacks: {
    async jwt(props) {
      const { token, account } = props;
      // Persist the OAuth access_token to the token right after signin
      console.log('-----------jwt', props);
      if (account) {
        token.accessToken = account.access_token;
        // token.userRole = 'admin';
      }
      return token;
    },
    // async session(props) {
    //   const { session, token, user } = props;
    //   console.log('-----------session', props);
    //   // Send properties to the client, like an access_token from a provider.
    //   // session.accessToken = token.accessToken;
    //   return session;
    // },
    // //回调函数
    // async signIn({ user, account, profile, email, credentials }) {
    //   console.log('-----------auth');
    //   console.log(email, credentials);
    //   //登录回调，如果authorize不成功，重定向到login界面，并附带错误信息参数
    //   if (!user) {
    //     return '/login/?msg=invalid';
    //   }
    //   return true;
    // },
  },
  pages: {
    //自定义界面 ，可配置signIn，signOut，error，verifyRequest，newUser
    signIn: '/login',
  },
};

export default NextAuth(authOptions);
