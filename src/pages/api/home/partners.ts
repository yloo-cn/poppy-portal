import { PartnersData } from '@/components/Partners';
import { ResultDto } from '@/types';
import type { NextApiRequest, NextApiResponse } from 'next';

const data = [
  { image: '/images/partners/tencent-cloud.svg', url: '#' },
  { image: '/images/partners/wxzf.svg', url: '#' },
  { image: '/images/partners/wxxcx.svg', url: '#' },
  { image: '/images/partners/aliyun.png', url: '#' },
  { image: '/images/partners/alipay.svg', url: '#' },
  { image: '/images/partners/zgyl.png', url: '#' },
  { image: '/images/partners/oschina.png', url: '#' },
  { image: '/images/partners/antd-ui.svg', url: '#' },
  { image: '/images/partners/nextjs.svg', url: '#' },
  { image: '/images/partners/github.svg', url: '#' },
  { image: '/images/partners/gitee.png', url: '#' },
  { image: '/images/partners/villatianna.png', url: 'https://villatianna.com/' },
];

export const slideshowResult: ResultDto<PartnersData[]> = { code: 200, data, success: true, message: '查询成功！' };

export default function handler(req: NextApiRequest, res: NextApiResponse<ResultDto<PartnersData[]>>) {
  res.status(200).json(slideshowResult);
}
