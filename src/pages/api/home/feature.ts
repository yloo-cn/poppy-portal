import { FeatureData } from '@/components/Feature';
import { ResultDto } from '@/types';
import type { NextApiRequest, NextApiResponse } from 'next';

const data = [
  {
    icon: 'app-list',
    title: '功能丰富',
    description: '包含低代码平台、用户、机构、岗位、角色、菜单、应用、字典等基础管理功能、个性化配置、模板、定时任务、导入导出、等20多种功能。',
  },
  {
    icon: 'laptop',
    title: '多端支持',
    description: '采用前后的分离架构，基于HTML5+CSS3响应式设计，支持平板、智能手机、PC等自动适配，支持各种常见的浏览器(不支持IE)',
  },
  {
    icon: 'safety',
    title: '信创适配',
    description: '实现对国产CPU、操作系统、数据库以及各类中间件等信创平台的全面兼容适配，支持达梦、人大金仓等国产数据库。',
  },
  {
    icon: 'lock',
    title: '权限管理',
    description: '包含功能权限和数据权限，可细粒度控制菜单、操作、数据范围、数据字段等，支持无限分级权限控制、可自由分配子级权限。',
  },
  {
    icon: 'app-edit',
    title: '定制开发',
    description: '完善的开发文档，合理的开发规范，代码开源、注释清晰，以及模块化的分层设计，细粒度的组件封装，可轻松实现高度个性化定制需求。',
  },
  {
    icon: 'code',
    title: '低代码平台',
    description: '集表单、大屏、流程等设计器，实现可视化配置，可通过数据模型、页面模型等方式，一键生成前后端代码、菜单及权限数据等功能。',
  },
  {
    icon: 'chrome',
    title: '服务端渲染',
    description: '基于Next.js框架实现预渲染，支持静态站点生成、服务端渲染、增量静态再生等，高效首屏显示性能，更好的SEO优化。',
  },
  {
    icon: 'app-add',
    title: '模块化',
    description: '系统高度组件化、模块化，可轻松组合个性化的产品需求，统一的视觉交互体验，灵活的扩展性，最大化提示开发效率和用户体验。',
  },
  {
    icon: 'translation',
    title: '国际化',
    description: '整站支持国际化，支持前端视图及后端数据的国际化，统一的国际化信息；默认支持中英文，可根据需要扩展多种语言。',
  },
];

export const featureData: ResultDto<FeatureData[]> = {
  code: 200,
  data,
  success: true,
  message: '查询成功！',
};

export default function handler(req: NextApiRequest, res: NextApiResponse<ResultDto<FeatureData[]>>) {
  res.status(200).json(featureData);
}
