// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { CasesData } from '@/components/Cases';
import { ResultDto } from '@/types';
import type { NextApiRequest, NextApiResponse } from 'next';

const data = [
  {
    image: '/images/product/product-1.webp',
    title: '义陆低代码平台',
    description: '基于SpringBoot + MybatisPlus + SpringSecurity + Vue的代码生成平台',
    url: 'https://demo.yloo.cn',
  },
  {
    image: '/images/product/product-2.webp',
    title: 'Poppy UI',
    description: '基于React + umijs + antd的后台管理系统组件库.',
    url: 'https://doc.yloo.cn',
  },
  {
    image: '/images/product/product-3.webp',
    title: 'Poppy Admin',
    description: '基于Poppy UI的微前端架构的企业级管理系统.通用型后台管理系统开发框架，界面美观、开箱即用、多终端支持拥有丰富的扩展组件和案例，欢迎企业和个人开发者使用！',
    url: 'https://gitee.com/yloo-cn/poppy-portal',
  },
];

export const slideshowResult: ResultDto<CasesData[]> = {
  code: 200,
  data,
  success: true,
  message: '查询成功！',
};

export default function handler(req: NextApiRequest, res: NextApiResponse<ResultDto<CasesData[]>>) {
  res.status(200).json(slideshowResult);
}
