import { ResultDto } from '@/types';
import type { NextApiRequest, NextApiResponse } from 'next';

export const featureData: ResultDto<any> = {
  code: 200,
  data: { token: 'token', refreshToken: 'refreshToken' },
  success: true,
  message: '查询成功！',
};

export default function handler(req: NextApiRequest, res: NextApiResponse<ResultDto<any>>) {
  res.status(200).json(featureData);
}
