import React, { useEffect } from 'react';
import { getLogDB } from '@/common/storage/LogDB';
import { isBrowser } from '@/utils/OsUtil';

export default function Download() {
  useEffect(() => {
    if (isBrowser()) {
      getLogDB().exports();
    }
  }, []);

  return <></>;
}
