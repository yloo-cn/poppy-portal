import Head from 'next/head';
import { useTranslation } from 'react-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import Index from '@/components/Advantage';
import BannerSlideshow from '@/components/BannerSlideshow';
import Feature from '@/components/Feature';
import Footer from '@/components/Footer';
import Header from '@/components/Header';
import Online from '@/components/Online';
import Partners from '@/components/Partners';
import Price from '@/components/Price';
import ProductPreview from '@/components/ProductPreview';
import { getPortalLayout } from '@/layouts';
import request from '@/utils/request';
import PaidService from '@/components/PaidService';
import Cases from '@/components/Cases';

export default function Home(props: any) {
  const { t } = useTranslation('common'); // key则是我们所创建的JSON文件的名称
  const { slideshowData, featureData } = props;
  return (
    <>
      <BannerSlideshow t={t} data={slideshowData} />
      <Feature data={featureData} />
      <Index />
      <ProductPreview />
      {/* <Cases /> */}
      <Price />
      <Partners />
      <PaidService />
      {/* <div id="preloader"></div> */}
    </>
  );
}

Home.getLayout = getPortalLayout;
// export async function getStaticProps({ locale }: any) {
//   return {
//     props: {
//       ...(await serverSideTranslations(locale, ['common', 'footer'])),
//       slideshowData: slideshowResult.data,
//     },
//   };
// }

export async function getServerSideProps({ locale }: any) {
  let bannerResult = await request.get('http://localhost:3000/api/home/bannerSlideshow');

  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'footer'])),
      slideshowData: bannerResult?.data || [],
    },
  };
}
