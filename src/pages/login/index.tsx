import { ReactNode } from 'react';
import Head from 'next/head';

import Copyright from '@/components/Footer/Copyright';
import { Button, Checkbox, Col, Form, Input, Row, Tabs } from 'antd';
import React, { useEffect, useState } from 'react';
import IconFont from '@/components/Icons';
import { useRequest, useSetState } from 'ahooks';
import { apiLogin, apiCaptcha, apiFakeCaptcha } from '@/services/login';
import { i18n, useTranslation } from 'next-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { getSession, getCsrfToken, signIn, getProviders } from 'next-auth/react';
import { signOut, useSession } from 'next-auth/react';
import { useRouter } from 'next/router';

interface Props {
  csrfToken: any;
  providers: any;
}
const Index = (props: Props) => {
  const [form] = Form.useForm();
  const [, forceUpdate] = useState({});
  const { t } = useTranslation(['common', 'login']); //key则是我们所创建的JSON文件的名称
  const { data: session, status } = useSession();
  const router = useRouter();
  const msg = router?.query?.msg; // 解析的错误信息  参数
  // To disable submit button at the beginning.
  useEffect(() => {
    forceUpdate({});
  }, []);
  const { loading, runAsync: onLogin } = useRequest(apiLogin, { manual: true });
  const { data: captchaData, runAsync: refreshCaptcha } = useRequest(apiCaptcha);
  const { captcha, image: captchaImage } = captchaData || ({} as any);

  const [loginParams, setLoginParams] = useSetState<any>({
    loginType: 'accountLogin',
    autoLogin: true,
    username: 'admin',
    password: 'pwd123',
  });

  const { loginType } = loginParams;
  // const intl = useIntl();
  const { providers } = props;
  console.log(providers);

  const [hasCaptcha, setHasCaptcha] = useState(false);
  let [countdown, setCountdown] = useState(15);
  const [disabled, setDisabled] = useState(false);
  let timer: any = null;

  const submitForm = () => {
    // console.log(form);
    // form.submit() 提交表单
    form
      .validateFields()
      .then(async (value) => {
        console.log(value);
        // ajax
        // let res = await ajax.verifycaptcha(value);
        // if (res.code == 200) {
        //   localStorage.setItem('phone', value.phone);
        //   sessionStorage.setItem('token', res.token);
        //   router('/main');
        // }
      })
      .catch(() => {
        // message.error('请输入有效的登录信息' + err);
      });
  };
  useEffect(() => {
    const phone = new URLSearchParams(location.search).get('phone');
    if (phone) {
      form.setFieldsValue({ phone: phone });
    } else {
      form.setFieldsValue({ phone: localStorage.getItem('phone') });
    }
    // setDisabled(reg.phone.test(form.getFieldValue('phone')));
  }, []);

  const checkDisabled = () => {
    // setDisabled(reg.phone.test(form.getFieldValue('phone')));
  };

  const sendCaptcha = async () => {
    const a = form.getFieldError('mobile');
    console.log(a);
    countDown();
    // let res = await ajax.sendcaptcha({ phone: form.getFieldValue('phone') });
  };
  const countDown = () => {
    setHasCaptcha(true);
    setCountdown(countdown--);
    timer = setInterval(() => {
      if (countdown > 0) {
        setHasCaptcha(true);
        setCountdown(countdown--);
      } else {
        clearInterval(timer);
        timer = null;
        setHasCaptcha(false);
        setCountdown(15);
      }
    }, 1000);
  };

  useEffect(() => {
    // if (isLogin()) {
    //   goto();
    // }
  }, []);

  useEffect(() => {
    setLoginParams({ captcha });
  }, [captcha, setLoginParams]);

  useEffect(() => {
    form.setFieldsValue(loginParams);
  }, [form, loginParams]);

  const setLoginType = (newType: string) => {
    setLoginParams({ loginType: newType });
  };
  // form.validateFields((nameList) => {
  //   // nameList
  //   nameList
  // });
  console.log(t('login:username'));
  const onFinish = (values: any) => {
    console.log('Finish:', values, loginParams);
    // onLogin({ ...params, loginType }).then((result) => {
    //   // console.log('-----', result);
    //   if (result.success) {
    //     message.success(result.message);
    //     const data = result.data || ({} as LoginResult);
    //     loginSession({
    //       loginType: loginType,
    //       autoLogin: params.autoLogin,
    //       username: params.username,
    //       ...data,
    //     });
    //     goto();
    //   } else if (result.message) {
    //     message.error(result.message);
    //   }
    // });
  };
  return (
    <>
      <Head>
        <title>登录-义陆低代码平台</title>
        <meta name="description" content="义陆低代码平台" />
        <link rel="icon" href="/icons/logo.svg" />
      </Head>
      {msg && <p className="xl-error-msg">{msg}</p>}
      {session?.user && (
        <>
          {session.user.image && <img src={session.user.image} />}
          <br />
          <strong>{session.user.email ?? session.user.name}</strong>
          <a
            href={`/api/auth/signout`}
            onClick={(e) => {
              e.preventDefault();
              signOut();
            }}
          >
            Sign out
          </a>
        </>
      )}
      <article className="login">
        <section className="login-body">
          <header className="header">
            <IconFont icon="yloo" className="logo" />
            <span>{t('site_title')}</span>
            <IconFont icon="translation" size={18} style={{ marginLeft: '1rem' }} />
            {/* <span className="lang">{SelectLang && <SelectLang />}</span> */}
          </header>
          <Tabs activeKey={loginType} onChange={setLoginType}>
            <Tabs.TabPane key="accountLogin" tab={t('login:accountLogin')} />
            <Tabs.TabPane key="mobileLogin" tab={t('login:mobileLogin')} />
          </Tabs>
          <Form className="login-form" size="large" form={form} onFinish={onFinish}>
            {loginType === 'accountLogin' && (
              <>
                <Form.Item
                  name="username"
                  rules={[{ required: true, message: t('login:username_valid') || '请输入用户名!' }]}
                >
                  <Input
                    placeholder={(t('login:username') || '用户名') + ': admin or user'}
                    prefix={<IconFont icon="user" className="text-primary" />}
                  />
                </Form.Item>
                <Form.Item
                  name="password"
                  rules={[{ required: true, message: t('login:password_valid') || '请输入密码!' }]}
                >
                  <Input.Password
                    placeholder={(t('login:password') || '密码') + ': admin'}
                    prefix={<IconFont icon="lock" className="text-primary" />}
                  />
                </Form.Item>
                <div className="flex gap-2">
                  <div style={{ maxWidth: 190 }}>
                    <Form.Item
                      name="captcha"
                      rules={[{ required: true, message: t('login:captcha_valid') || '请输入验证码!' }]}
                    >
                      <Input
                        placeholder={t('login:captcha') || '验证码'}
                        prefix={<IconFont icon="key" className="text-primary" />}
                      />
                    </Form.Item>
                  </div>
                  <span style={{ minWidth: 128 }}>
                    <img className="login-code" onClick={refreshCaptcha} src={captchaImage} />
                  </span>
                </div>
              </>
            )}
            {loginType === 'mobileLogin' && (
              <>
                <Form.Item
                  name="mobile"
                  rules={[
                    { required: true, message: t('login:mobile_valid') || '请输入手机号!' },
                    { pattern: /^1\d{10}$/, message: t('login:mobile_error_valid') || '手机号格式错误！!' },
                    (a) => ({
                      validator(_, value, calback) {
                        const { getFieldValue } = a;
                        console.log('a, _, value', a, _.message, value, calback);
                        return Promise.resolve();
                      },
                    }),
                  ]}
                >
                  <Input
                    placeholder={t('login:mobile') || '手机号'}
                    prefix={<IconFont icon="mobile" className="text-primary" />}
                    onChange={() => {
                      console.log(
                        "form.getFieldError('mobile').length === 0",
                        form.getFieldError('mobile').length === 0,
                      );
                      setDisabled(form.getFieldError('mobile').length === 0);
                    }}
                  />
                </Form.Item>
                <Form.Item
                  name="captcha"
                  rules={[{ required: true, message: t('login:captcha_valid') || '请输入验证码!' }]}
                >
                  <Input
                    placeholder={t('login:captcha') || '验证码'}
                    prefix={<IconFont icon="safety" className="text-primary" />}
                  />
                </Form.Item>

                <div className="flex gap-2">
                  <div style={{ maxWidth: 190 }}>
                    <Form.Item
                      name="captcha"
                      rules={[{ required: true, message: t('login:captcha_valid') || '请输入验证码!' }]}
                    >
                      <Input
                        placeholder={t('login:captcha') || '验证码'}
                        prefix={<IconFont icon="safety" className="text-primary" />}
                      />
                    </Form.Item>
                  </div>
                  <span style={{ minWidth: 128 }}>
                    <Button onClick={sendCaptcha} block disabled={disabled}>
                      {!hasCaptcha
                        ? t('login:get_captcha') || '获取验证码'
                        : (t('login:excess_time') || '剩余') + ` ${countdown} S`}
                    </Button>
                  </span>
                </div>
                {/* <ProFormCaptcha
                  fieldProps={{}}
                  captchaTextRender={(timing: any, count: any) => {
                    if (timing) {
                      return `${count} ${intl.formatMessage({
                        id: 'pages.getCaptchaSecondText',
                        defaultMessage: '获取验证码',
                      })}`;
                    }
                    return intl.formatMessage({
                      id: 'pages.login.phoneLogin.getVerificationCode',
                      defaultMessage: '获取验证码',
                    });
                  }}
                  name="captcha"
                  rules={[
                    {
                      required: true,
                      message: '<FormattedMessage id="pages.login.captcha.required" defaultMessage="" />',
                    },
                  ]}
                  onGetCaptcha={async (phone: any) => {
                    const result = await apiFakeCaptcha({
                      phone,
                    });
                    if (result === false) {
                      return;
                    }
                    message.success('获取验证码成功！验证码为：1234');
                  }}
                /> */}
              </>
            )}
            <div className="flex justify-between mb-4">
              <Form.Item name="autoLogin" valuePropName="checked" noStyle>
                <Checkbox>{t('login:auto_login')}</Checkbox>
              </Form.Item>
              <a>{t('login:forget_password')}</a>
            </div>
            <Button className="mb-6" type="primary" block htmlType="submit" loading={loading}>
              {t('login:login')}
            </Button>
          </Form>
          <div className="other-login ">
            {t('login:other_login')}:
            <form action="/api/auth/signin/wechat" method="post">
              <input name="csrfToken" type="hidden" defaultValue={props.csrfToken} />
              <button type="submit">
                <IconFont icon="wechat-circle-fill" className="other-login-icon" />
              </button>
            </form>
            <form action="/api/auth/signin/qq" method="post">
              <input name="csrfToken" type="hidden" defaultValue={props.csrfToken} />
              <button type="submit">
                <IconFont icon="qq-circle-fill" className="other-login-icon" />
              </button>
            </form>
            <form action="/api/auth/signin/alipay" method="post">
              <input name="csrfToken" type="hidden" defaultValue={props.csrfToken} />
              <button type="submit">
                <IconFont icon="alipay-circle" className="other-login-icon" />
              </button>
            </form>
            {Object.values(providers).map((provider: any) => {
              // eslint-disable-next-line react/jsx-key
              return (
                <IconFont
                  key={provider.id}
                  onClick={() => signIn(provider.id)}
                  icon={provider.name}
                  className="other-login-icon"
                />
              );
            })}
          </div>
        </section>
        <footer className="login-footer">
          <Copyright />
        </footer>
      </article>
    </>
  );
};

export default Index;

// export async function getInitialProps(context: any) {

// }

export async function getServerSideProps(context: any) {
  const { locale, req, res } = context;
  const session = await getSession({ req });
  if (session && res && session.user) {
    res.writeHead(302, {
      Location: '/',
    });
    res.end();
    return;
  }
  const providers = await getProviders();
  return {
    props: {
      // session: undefined,
      // providers: await getProviders(),
      ...(await serverSideTranslations(locale, ['common', 'login'])),
      providers,
      csrfToken: await getCsrfToken(context),
    },
  };
  // return {
  //   props: {
  //     ...(await serverSideTranslations(locale, ['common', 'login'])),
  //   },
  // };
}

// import React, { useEffect, useReducer } from 'react';

// import { message, Tabs, Form, Input, Button, Checkbox, Row, Col } from 'antd';

// // import ulog from '@babel-macro-datayes/ulog.macro';

// import { ProFormCaptcha, ProFormText } from '@ant-design/pro-form';
// // import { useIntl, history, FormattedMessage, SelectLang } from 'umi';
// import { useRequest, useSetState } from 'ahooks';

// // import Footer from '@/components/Footer';
// // import { apiLogin, apiCaptcha, apiFakeCaptcha } from '@/services/login';
// // import type { LoginParams, LoginResult } from '@/services/types';
// // import { createBasicSchemaField, QF } from '@yloo/poppy-formily';

// import './index.less';
// import { getAppContext } from '@/common/config';

// /** 此方法会跳转到 redirect 参数所在的位置 */
// const goto = () => {
//   if (!history) return;
//   setTimeout(() => {
//     // const { query } = location;
//     // const { redirect } = query as { redirect: string };
//     // history.push(redirect || '/');
//   }, 10);
// };

// const { isLogin, loginSession } = getAppContext();

// /**
//  * 获取强制更新当前组件方法
//  * @returns 强制更新当前组件方法
//  */
// export function useForceUpdate() {
//   const forceUpdate = useReducer((x) => x + 1, 0)[1];

//   return forceUpdate;
// }

Index.getLayout = (children: ReactNode) => {
  return <>{children}</>;
};
