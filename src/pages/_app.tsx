import { ReactElement } from 'react';
import type { NextPage } from 'next';
import type { AppProps } from 'next/app';
import { appWithTranslation } from 'next-i18next';
import { useTranslation } from 'react-i18next';
import { SessionProvider } from 'next-auth/react';

import '../styles/tailwind.css';
import '../styles/globals.css';
import '../styles/index.scss';
import { getPortalLayout } from '@/layouts';
import Head from 'next/head';

type NextPageWithLayout = NextPage & {
  getLayout?: (page: ReactElement) => ReactElement;
};

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
};

const App = ({ Component, pageProps: { session, ...pageProps } }: AppPropsWithLayout) => {
  // key则是我们所创建的JSON文件的名称
  const { t } = useTranslation('common');
  let getLayout = Component.getLayout;
  if (!getLayout) {
    getLayout = (page) => page;
    getLayout = (page) => page;
    if (pageProps?.statusCode) {
      /* empty */
    } else {
      getLayout = getPortalLayout;
    }
  }
  return (
    <>
      <Head>
        <title>{t('site_title')}</title>
        <meta name="description" content={t('site_description') || '义陆源码,义陆,创新平台,信创,低代码,低代码平台,小程序开发,公众号开发,yloo,bgxcx'} />
        <link rel="icon" href="/icons/logo.svg" />
      </Head>
      <SessionProvider
        // options={{
        //   staleTime: 0,
        //   refetchInterval: 0,
        // }}
        session={session}
      >
        {getLayout(<Component i18n={t} {...pageProps} />)}
      </SessionProvider>
    </>
  );
};

export default appWithTranslation(App);
