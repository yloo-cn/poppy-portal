import Head from 'next/head';
import { useTranslation } from 'react-i18next';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import Index from '@/components/Advantage';
import BannerSlideshow from '@/components/BannerSlideshow';
import Feature from '@/components/Feature';
import Footer from '@/components/Footer';
import Header from '@/components/Header';
import Online from '@/components/Online';
import Partners from '@/components/Partners';
import Price from '@/components/Price';
import ProductPreview from '@/components/ProductPreview';
import { slideshowResult } from '@/pages/api/home/bannerSlideshow';
import request from '@/utils/request';
import { getPortalLayout } from '@/layouts';
import { ReactElement } from 'react';

export default function News(props: any) {
  const { t } = useTranslation('common'); //key则是我们所创建的JSON文件的名称
  const { slideshowData } = props;
  return (
    <>
      <Head>
        <title>新闻-义陆源码</title>
      </Head>
      <Feature />
      <Index />
      <Price />
      <Partners />
      <Online />
      {/* <div id="preloader"></div> */}
    </>
  );
}

// News.getLayout = (children: ReactElement)=> {
//   return <>{children}</>;
// };

export async function getServerSideProps({ locale }: any) {
  let result = await request.get('http://localhost:3000/api/home/bannerSlideshow');
  // eslint-disable-next-line no-console
  console.log('--------------');
  // const {
  //   data: { data },
  // } = result;
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'footer'])),
      slideshowData: result.data, //props值传导render函数中
    },
  };
}
