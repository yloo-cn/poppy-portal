
import request from '@/utils/request';
import type { Captcha, FakeCaptcha, LoginParams, LoginResult, ResultDto } from '../types';

/**
 * 登录接口 POST /api/login
 * @param data
 */
export async function apiLogin(data: LoginParams) {
  return request<ResultDto<LoginResult>>('/api/login', {
    method: 'POST',
    data,
  });
}

/**
 * 退出接口 POST /api/logout
 */
export async function apiLogout(options?: Record<string, any>) {
  return request<Record<string, any>>('/api/logout', {
    method: 'POST',
    ...(options || {}),
  });
}

/**
 * 获取图片验证码 POST /api/login/captcha
 */
export async function apiCaptcha(options?: Record<string, any>) {
  return request<ResultDto<Captcha>>('/api/login/captcha', {
    method: 'GET',
    ...(options || {}),
  }).then((result) => {
    return result?.data;
  });
}

/**
 * 发送验证码 POST /api/login/captcha
 * @param params
 */
export async function apiFakeCaptcha(params: { phone?: string }, options?: Record<string, any>) {
  return request<FakeCaptcha>('/api/login/captcha', {
    method: 'POST',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}
