import { IconFont } from '../Icons';

const data = [
  {
    icon: 'customizing',
    title: '定制开发',
    description: 'APP、H5、小程序、公众号、网站建设、企业应用等程序定制开发，系统运维等。',
  },
  {
    icon: 'remote-desktop',
    title: '远程协助',
    description: '远程bug分析及解决，系统安全、性能调优，解决方案及技术调研，远程编码。',
  },
  {
    icon: 'company-service',
    title: '创业服务',
    description: '公司注册、代理记账、商标、名片设计，企业策划和推广，完善公司相关章程文档。',
  },
  {
    icon: 'course',
    title: '课程设计',
    description: '系统源码开发、概要设计，详细设计，用户手册、论文等文档指导。',
  },
];

interface Props {}

const Index: React.FC<Props> = (props) => {
  return (
    <section className="paid-service">
      <div className="container">
        <div className="title-section">
          <h2>有偿服务</h2>
        </div>
        <ul className="grid md:grid-cols-2 xl:grid-cols-4 gap-4 xl:gap-8">
          {data.map((item) => {
            return (
              <li key={item.title}>
                <div>
                  <h3>
                    <span className="feature-icon">
                      <IconFont icon={item.icon} />
                    </span>
                    {item.title}
                  </h3>
                  <p>{item.description}</p>
                </div>
              </li>
            );
          })}
        </ul>
      </div>
    </section>
  );
};

export default Index;
