import Image from 'next/image';
import { SwiperSlide } from 'swiper/react';
import SwiperWrapper from '../SwiperWrapper';

const data = [
  {
    image: '/images/product/1.png',
  },
  {
    image: '/images/product/2.png',
  },
  {
    image: '/images/product/3.png',
  },
  {
    image: '/images/product/5.png',
  },
  {
    image: '/images/product/1.png',
  },
  {
    image: '/images/product/3.png',
  },
];

interface Props {
  title: string;
  description?: string;
}
export default function Index(props: Props) {
  const { title = '产品展示', description } = props;
  return (
    <section className="product-preview box-section">
      <div className="container">
        <div className="title-section">
          <h2>{title}</h2>
          {description && <p>{description}</p>}
        </div>
        <SwiperWrapper
          className="banner-slideshow"
          slidesPerView={'auto'}
          spaceBetween={64}
          slidesPerGroup={1}
          autoplay={{
            delay: 10000000,
            disableOnInteraction: false,
          }}
          speed={500}
          navigation={false}
          centeredSlides={false}
          slidesPerGroupSkip={1}
        >
          {data.map((item) => {
            return (
              <SwiperSlide key={item.image}>
                <div className="case-image">
                  <Image width={900} height={450} src={item.image} alt={'product'} />
                </div>
              </SwiperSlide>
            );
          })}
        </SwiperWrapper>
      </div>
    </section>
  );
}
