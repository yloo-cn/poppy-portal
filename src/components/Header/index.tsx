import Link from 'next/link';

import { useTranslation } from 'next-i18next';
import IconFont, { SvgIcon } from '../Icons';

import Lu from '@/icons/fonts/lu.svg';
import Yi from '@/icons/fonts/yi.svg';
import { useRouter } from 'next/router';
import { signOut, useSession } from 'next-auth/react';

export default function Index() {
  const { t } = useTranslation('common'); //key则是我们所创建的JSON文件的名称
  const router = useRouter();
  const { data: session, status } = useSession();

  const handleLocaleChange = (event: any) => {
    const value = event.target.value;
    router.push(router.route, router.asPath, {
      locale: value,
    });
  };
  const { locale, locales, defaultLocale, asPath } = useRouter();

  return (
    <>
      <header className="header-container active">
        <nav className="container header-wrapper md:gap-2 lg:gap-8">
          <Link href="/" className="logo">
            <SvgIcon>
              <Yi />
            </SvgIcon>
            <SvgIcon>
              <Lu />
            </SvgIcon>
            {/* <span className="hidden lg:block">平台</span> */}
          </Link>
          <ul className="header-menu sm:gap-2 lg:gap-8">
            <li>
              <Link href="/">首页</Link>
            </li>
            <li>
              <Link href="#">产品方案</Link>
              <div className="drowdown-hover">
                <Link target="_blank" href="https://demo.yloo.cn">
                  低代码平台
                </Link>
                <Link target="_blank" href="https://doc.yloo.cn">
                  义陆组件库
                </Link>
                <Link target="_blank" href="https://doc.yloo.cn">
                  表单设计器
                </Link>
                <Link target="_blank" href="https://doc.yloo.cn">
                  大屏设计器
                </Link>
                <Link target="_blank" href="https://doc.yloo.cn">
                  移动小程序
                </Link>
              </div>
            </li>
            <li>
              <Link href="#">商务合作</Link>
              <div className="drowdown-hover">
                <Link target="_blank" href="https://doc.yloo.cn">
                  申请试用
                </Link>
                <Link target="_blank" href="https://doc.yloo.cn">
                  商业授权
                </Link>
                <Link target="_blank" href="https://doc.yloo.cn">
                  定制开发
                </Link>
                <Link target="_blank" href="https://doc.yloo.cn">
                  赞助投资
                </Link>
                <Link target="_blank" href="https://doc.yloo.cn">
                  广告服务
                </Link>
              </div>
            </li>
            <li>
              <a href="#">技术支持</a>
              <div className="drowdown-hover">
                <Link target="_blank" href="https://doc.yloo.cn">
                  开发手册
                </Link>
                <Link target="_blank" href="https://doc.yloo.cn">
                  部署手册
                </Link>
                <Link target="_blank" href="https://doc.yloo.cn">
                  常见问题
                </Link>
                <Link target="_blank" href="https://doc.yloo.cn">
                  入门视频
                </Link>
                <Link target="_blank" href="https://doc.yloo.cn">
                  开源项目
                </Link>
              </div>
            </li>
            <li>
              <Link href="#">新闻动态</Link>
              <div className="drowdown-hover">
                <Link href="/news">产品动态</Link>
                <Link href="/news">最新资讯</Link>
                <Link href="/news">活动特惠</Link>
              </div>
            </li>
            <li>
              <a>关于我们</a>
              <div className="drowdown-hover">
                <Link target="_blank" href="/">
                  公司简介
                </Link>
                <Link target="_blank" href="/">
                  发展历程
                </Link>
                <Link target="_blank" href="/">
                  招贤纳士
                </Link>
                <Link target="_blank" href="/">
                  联系我们
                </Link>
              </div>
            </li>
          </ul>
          <ul className="header-tool">
            <li>
              <Link href="/">
                <IconFont icon="bell" />
              </Link>
              {/* <ul className="chart-scroll">
                <li>
                  <div className="cart-single-product">
                    <div className="media">
                      <div className="pull-left cart-product-img">
                        <a href="#">
                          <img className="media-object" src="/images/product/product-3.jpg" alt="product" />
                        </a>
                      </div>
                      <div className="media-body cart-content">
                        <h4 className="media-heading">
                          <a href="#">Product Title Here</a>
                        </h4>
                        <a href="#" className="trash">
                          <i className="fa fa-trash-o"></i>
                        </a>
                        <p>Quantity : 1</p>
                        <div className="cart-product-price">
                          <span>$49</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div className="cart-single-product">
                    <div className="media">
                      <div className="pull-left cart-product-img">
                        <a href="#">
                          <img className="media-object" src="/images/product/product-2.jpg" alt="product" />
                        </a>
                      </div>
                      <div className="media-body cart-content">
                        <h4 className="media-heading">
                          <a href="#">Product Title Here</a>
                        </h4>
                        <a href="#" className="trash">
                          <i className="fa fa-trash-o"></i>
                        </a>
                        <p>Quantity : 1</p>
                        <div className="cart-product-price">
                          <span>$99</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <a href="checkout.html" className="checkout-button">
                    Checkout
                  </a>
                </li>
              </ul> */}
            </li>
            <li>
              <Link href="/">
                <IconFont icon="shopping-cart" />
              </Link>
            </li>
            <li>
              <Link href="/">
                <IconFont
                  icon="translation"
                  onClick={(e) => {
                    handleLocaleChange(e);
                  }}
                />
              </Link>
              {/* <ul className="chart-scroll">
                <li>
                  <Link href={asPath} locale="zh">
                    简体中文
                  </Link>
                </li>
                <li>
                  <Link href={asPath} locale="en">
                    English
                  </Link>
                </li>
              </ul> */}
            </li>
            <li>
              <Link href="/">
                <IconFont icon="account" />
                {session?.user && (
                  <>
                    {session.user.image && <span style={{ backgroundImage: `url('${session.user.image}')` }} />}
                    <small>Signed in as</small>
                    <br />
                    <strong>{session.user.email ?? session.user.name}</strong>
                    <a
                      href={'/api/auth/signout'}
                      onClick={(e) => {
                        e.preventDefault();
                      }}
                    >
                      Sign out
                    </a>
                  </>
                )}
              </Link>
              {/* <ul className="chart-scroll">
                <li>
                  <Link href="" onClick={() => signOut()}>
                    用户退出
                  </Link>
                </li>
                <li>
                  <Link href="/login">用户登录</Link>
                </li>
                <li>
                  <Link target="_blank" href="checkout.html" className="checkout-button">
                    Checkout
                  </Link>
                </li>
              </ul> */}
            </li>
            <li className=" lg:hidden">
              <Link href="/">
                <IconFont icon="menu" />
              </Link>
              {/* <ul className="chart-scroll">
                <li>
                  <Link target="_blank" href="https://doc.yloo.cn">
                    用户退出
                  </Link>
                </li>
                <li>
                  <Link target="_blank" href="https://doc.yloo.cn/doc.html">
                    后端Swagger接口
                  </Link>
                </li>
                <li>
                  <Link target="_blank" href="checkout.html" className="checkout-button">
                    Checkout
                  </Link>
                </li>
              </ul> */}
            </li>
          </ul>
        </nav>
      </header>
      {/* <Menu /> */}
    </>
  );
}
