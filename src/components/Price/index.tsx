import React from 'react';

interface Props {
  data?: [];
}

const Index: React.FC<Props> = (props) => {
  return (
    <section className="product-price box-section">
      <div className="container">
        <div className="title-section">
          <h2>产品价格</h2>
          <p>比同行更优惠</p>
        </div>
        <div className="grid md:grid-cols-3 gap-8 sm:grid-cols-1">
          <div className="pricing-table-item">
            <h4>开源社区版</h4>
            <h3>￥0</h3>
            <ul>
              <li>免费授权</li>
              <li>仅限个人非商业用途</li>
              <li>-</li>
              <li>-</li>
              <li>开源社区支持</li>
              <li>基础功能</li>
              <li>基础功能源码</li>
              <li>终身免费使用</li>
              <li>免费升级开源版</li>
              <li>QQ技术交流群</li>
              <li>保留版权标识不可删除</li>
              <li>-</li>
            </ul>
            <a className="yl-button btn-default" href="#">
              免费下载
            </a>
          </div>
          <div className="pricing-table-item">
            <h4>
              企业版（<s>1999</s>）
            </h4>
            <h3>活动价：￥199</h3>
            <ul>
              <li>商业授权</li>
              <li>限授权企业使用</li>
              <li>1个授权码</li>
              <li>1个顶级域名授权</li>
              <li>1年技术支持</li>
              <li>全部功能</li>
              <li>无低代码平台源码</li>
              <li>终身免费使用</li>
              <li>3年免费升级</li>
              <li>VIP技术交流群</li>
              <li>版权标识可删除</li>
              <li>升级定制9折起</li>
            </ul>
            <a className="yl-button btn-default" href="#">
              立即购买
            </a>
          </div>
          <div className="pricing-table-item">
            <h4>
              旗舰版（<s>￥9999</s>）
            </h4>
            <h3>活动价￥999</h3>
            <ul>
              <li>商业授权</li>
              <li>限授权企业使用</li>
              <li>不限授权码数量</li>
              <li>1个顶级域名授权</li>
              <li>2年技术支持</li>
              <li>全部功能</li>
              <li>全端源码</li>
              <li>终身免费使用</li>
              <li>3年免费升级</li>
              <li>VIP技术交流群</li>
              <li>版权标识可删除</li>
              <li>升级定制8折起</li>
            </ul>
            <a className="yl-button btn-default" href="#">
              立即购买
            </a>
          </div>
        </div>
        <div className="prompt-message mt-4">
          <p>1. 产品不可用于非法活动，包括但不限于赌博、诈骗等活动。</p>
          <p>2. 由于虚拟产品(100%可复制性)、授权源代码不可转让、转售、盗版贩卖，发现则取消授权不退款、情节严重将追究法律责任。</p>
          <p>3. 如授权人为公司主体、公司转卖则授权一起、可联系客服直接更换联系人即可、无需收取费用。</p>
        </div>
      </div>
    </section>
  );
};

export default Index;
