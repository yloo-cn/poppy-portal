import Image from 'next/image';
import Link from 'next/link';
import { IconFont } from '../Icons';

interface Props {}

const Index: React.FC<Props> = (props) => {
  return (
    <section className="footer-nav-container">
      <div className="container footer-nav-wrapper py-4 lg:py-8">
        <ul>
          <li>
            <h3 className="text-lg mb-2">产品中心</h3>
          </li>
          <li>
            <Link target="_blank" href="/">
              低代码平台
            </Link>
          </li>
          <li>
            <Link target="_blank" href="/">
              义陆组件库
            </Link>
          </li>
          <li>
            <Link target="_blank" href="/">
              表单设计器
            </Link>
          </li>
          <li>
            <Link target="_blank" href="/">
              移动小程序
            </Link>
          </li>
        </ul>
        <ul>
          <li>
            <h3 className="text-lg mb-2">服务支持</h3>
          </li>
          <li>
            <Link target="_blank" href="/">
              授权查询
            </Link>
          </li>
          <li>
            <Link target="_blank" href="/">
              购买授权
            </Link>
          </li>
          <li>
            <Link target="_blank" href="/">
              广告投放
            </Link>
          </li>
          <li>
            <Link target="_blank" href="/">
              申请链接
            </Link>
          </li>
          <li>
            <Link target="_blank" href="/">
              捐赠我们
            </Link>
          </li>
        </ul>
        <ul>
          <li>
            <h3 className="text-lg mb-2">技术支持</h3>
          </li>
          <li>
            <Link target="_blank" href="/">
              开发手册
            </Link>
          </li>
          <li>
            <Link target="_blank" href="/">
              运维手册
            </Link>
          </li>
          <li>
            <Link target="_blank" href="/">
              帮助中心
            </Link>
          </li>
          <li>
            <Link target="_blank" href="/">
              资源下载
            </Link>
          </li>
          <li>
            <Link target="_blank" href="/">
              问题反馈
            </Link>
          </li>
        </ul>
        <ul>
          <li>
            <h3 className="text-lg mb-2">法律声明</h3>
          </li>
          <li>
            <Link target="_blank" href="/">
              知识产权
            </Link>
          </li>
          <li>
            <Link target="_blank" href="/">
              免责声明
            </Link>
          </li>
          <li>
            <Link target="_blank" href="/">
              授权许可协议
            </Link>
          </li>
          <li>
            <Link target="_blank" href="/">
              用户服务协议
            </Link>
          </li>
          <li>
            <Link target="_blank" href="/">
              隐私保护政策
            </Link>
          </li>
        </ul>
        <ul>
          <li>
            <h3 className="text-lg mb-2">联系我们</h3>
          </li>
          <li>
            <IconFont icon="telephone-fill" />
            <span>电话：</span>
            <a href="tel:13666729219" title="点击拨打电话">
              13666729219
            </a>
          </li>
          <li>
            <IconFont icon="mail-fill" />
            <span>邮箱：</span>
            <a href="mailto:yloo-cn@qq.com" title="点击发送邮件">
              yloo-cn@qq.com
            </a>
          </li>
          <li>
            <IconFont icon="qq-fill" />
            <span>ＱＱ：</span>
            <a href="tencent://message/?Menu=yes&amp;uin=118118452" target="_blank" title="点击发起聊天" rel="noreferrer">
              118118452
            </a>
          </li>
          <li>
            <IconFont icon="wechat" />
            <span>微信：</span>yloo_cn
          </li>
          <li>
            <IconFont icon="environment-fill" />
            <span>Ｑ群：</span>16107586、107596819
          </li>
        </ul>
        <ul>
          <li>
            <span className="p-2 bg-white rounded-md">
              <Image width={160} height={160} src="/images/weixin.png" alt={'技术交流群'} />
            </span>
          </li>
          <li>
            <h3 className="text-lg text-center w-full">微信公众号</h3>
          </li>
        </ul>
      </div>
      <form className="hidden">
        <h3 className="text-lg mb-2">意见反馈</h3>
        <fieldset className="get-quote">
          <div className="form-group">
            <input id="quote-name" name="textinput" placeholder="Name*" className="form-control input-md" type="text" />
          </div>
          <div className="form-group">
            <input id="quote-email" name="textinput" placeholder="E-mail*" className="form-control input-md" type="text" />
          </div>
          <div className="form-group">
            <textarea id="textarea" name="textarea" placeholder="Message" className="form-control input-md" rows={3}></textarea>
          </div>
          <div className="form-group send-button">
            <button type="submit" className="ghost-btn">
              Submit
            </button>
          </div>
        </fieldset>
      </form>
    </section>
  );
};

export default Index;
