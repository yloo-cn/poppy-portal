import React from 'react';

interface Props {}

const Index: React.FC<Props> = (props) => {
  return (
    <section className="promote-wrap">
      <div className="container py-8 flex justify-between">
        <h3 className="text-3xl">搭建义陆私有化低代码开发平台，让开发更高效！</h3>
        <div className="button-area">
          <a className="yl-button btn-default" href="#">
            申请试用
          </a>
        </div>
      </div>
    </section>
  );
};

export default Index;
