import React from 'react';

interface Props {}

const Index: React.FC<Props> = (props) => {
  return (
    <a id="scrollTop" href="#top">
      <i className="fa fa-arrow-up"></i>
    </a>
  );
};

export default Index;
