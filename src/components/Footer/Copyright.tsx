import React from 'react';
import FriendLink from './FriendLink';

interface Props {}

const Index: React.FC<Props> = (props) => {
  return (
    <div className="container copyright-wrapper">
      Copyright © 2022&nbsp;
      <a href="https://yloo.cn/" target="_blank" rel="noreferrer">
        YLOO.CN
      </a>
      &nbsp;All Rights Reserved.&nbsp;&nbsp;
      <a href="https://beian.miit.gov.cn/" target="_blank" rel="noreferrer">
        浙ICP备2023005989号-2
      </a>
    </div>
  );
};

export default Index;
