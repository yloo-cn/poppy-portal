import PaidService from '../PaidService';
import Copyright from './Copyright';
import FooterNav from './FooterNav';
import FriendLink from './FriendLink';

interface Props {}

const Index: React.FC<Props> = (props) => {
  return (
    <footer className="footer-container">
      <FooterNav />
      <div className="copyright-container">
        <FriendLink />
        <Copyright />
      </div>
      {/* <ScrollTop /> */}
    </footer>
  );
};

export default Index;
