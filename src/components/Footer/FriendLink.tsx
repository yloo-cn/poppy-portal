import Link from 'next/link';

interface Props {}

const Index: React.FC<Props> = (props) => {
  return (
    <div className="container friend-link-wrapper">
      <span>友情链接:</span>
      <Link target="_blank" href="/">
        Poppy UI
      </Link>
      |
      <Link target="_blank" href="/">
        义陆门户
      </Link>
      |
      <Link target="_blank" href="/">
        许愿树
      </Link>
      |
      <Link target="_blank" href="/">
        Formily
      </Link>
    </div>
  );
};

export default Index;
