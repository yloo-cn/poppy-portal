import { SwiperSlide } from 'swiper/react';

import SwiperWrapper from '@/components/SwiperWrapper';
import SlideItem from './SlideItem';

export interface SlideshowData {
  bgColor: string;
  bgImage: string;
  image: string;
  title: string;
  url: string;
  description: string;
  downloadNow: string;
}

interface Props {
  t?: any;
  data?: SlideshowData[];
}

const Index: React.FC<Props> = (props) => {
  const { t, data = [] } = props;
  return (
    <>
      <SwiperWrapper className="banner-slideshow" navigationContainer>
        {data?.map((item: any) => {
          return (
            <SwiperSlide key={item.title}>
              <SlideItem
                bgColor={item.bgColor}
                bgImage={item.bgImage}
                image={item.image}
                title={item.title}
                description={item.description}
                url={item.url}
                downloadNow={item.downloadNow || `${t('download_now')}`}
              ></SlideItem>
            </SwiperSlide>
          );
        })}
      </SwiperWrapper>
    </>
  );
};

export default Index;
