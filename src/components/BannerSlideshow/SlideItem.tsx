import Image from 'next/image';
import Link from 'next/link';

interface Props extends Record<string, any> {
  width?: number;
  height?: number;

  bgColor: string;
  bgImage: string;
  image: string;
  title: string;
  url: string;
  description: string;
  downloadNow: string;
}

export default function SlideItem(props: Props) {
  const { width = 1920, height = 800, bgImage, image, title, description, url, downloadNow, bgColor, ...rest } = props;
  return (
    <>
      <div style={{ backgroundImage: `url(${bgImage})`, padding: '6% 0' }}>
        {/* <Image className="mx-auto" width={width} height={height} src={src} alt={title} /> */}
        <div className="slide-content-wrap container">
          <div className="flex flex-col w-3/5 md:pl-4 lg:pl-16 gap-4 lg:gap-6 ">
            <h1 className="text-3xl sm:text-4xl md:text-5xl lg:text-6xl xl:text-7xl font-bold">{title}</h1>
            <h4 className="text-base">{description}</h4>
            <span>
              <Link className="yl-button btn-default py-2 px-4 lg:py-2 xl:px-8" target="_blank" href={url}>
                {downloadNow}
              </Link>
            </span>
          </div>
          <div>
            <Image className="mx-auto" width={650} height={500} src={image} alt={title} />
          </div>
        </div>
      </div>
    </>
  );
}
