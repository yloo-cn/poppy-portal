import React, { useEffect } from 'react';
import Image from 'next/image';
import { useRequest } from 'ahooks';
import request from '@/utils/request';
import { ResultDto } from '@/types';
import Loading from '../Loading';
import Link from 'next/link';

export interface PartnersData {
  url?: string;
  image: string;
}

interface Props {
  title?: string;
  description?: string;
}

const Index: React.FC<Props> = (props) => {
  const { title = '合作伙伴', description = '强强联合，只为提供更好服务，我们坚持不懈，有你我们才能做的更好' } = props;
  const {
    data: result,
    loading,
    run,
  } = useRequest(() => request.get<ResultDto<PartnersData[]>, ResultDto<PartnersData[]>>('/api/home/partners'), {
    manual: true,
  });

  useEffect(() => {
    run();
  }, [run]);

  const data = result?.data;

  return (
    <section className="partners box-section">
      {loading && <Loading />}
      <div className="container">
        <div className="title-section">
          <h2>{title}</h2>
          <p>{description}</p>
        </div>
        <div className="grid grid-cols-3 sm:grid-cols-4 lg:grid-cols-6">
          {data?.map((item: any) => {
            return (
              <Link href={item.url} key={item.image} target="_blank">
                <Image width={320} height={120} src={item.image} alt={'logo'} />
              </Link>
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default Index;
