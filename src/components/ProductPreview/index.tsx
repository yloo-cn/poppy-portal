import Image from 'next/image';
import { SwiperSlide } from 'swiper/react';
import SwiperWrapper from '../SwiperWrapper';

const data = [
  {
    image: '/images/product/product-1.webp',
  },
  {
    image: '/images/product/product-2.webp',
  },
  {
    image: '/images/product/product-3.webp',
  },
  {
    image: '/images/product/product-4.webp',
  },
  {
    image: '/images/product/product-5.webp',
  },
  {
    image: '/images/product/product-6.webp',
  },
  {
    image: '/images/product/product-7.webp',
  },
  {
    image: '/images/product/product-8.webp',
  },
];

interface Props {
  title?: string;
  description?: string;
}

const Index: React.FC<Props> = (props) => {
  const { title = '产品展示', description } = props;
  return (
    <section className="product-preview box-section">
      <div className="container">
        <div className="title-section">
          <h2>{title}</h2>
          {description && <p>{description}</p>}
        </div>
        <SwiperWrapper
          className="banner-slideshow"
          slidesPerView={'auto'}
          spaceBetween={32}
          slidesPerGroup={1}
          autoplay={{
            delay: 1500,
            disableOnInteraction: false,
          }}
          speed={800}
          navigation={false}
          centeredSlides={false}
          slidesPerGroupSkip={1}
        >
          {data.map((item) => {
            return (
              <SwiperSlide key={item.image}>
                <div className="animate__animated animate__bounce">
                  <Image width={900} height={450} src={item.image} alt={'product'} />
                </div>
              </SwiperSlide>
            );
          })}
        </SwiperWrapper>
      </div>
    </section>
  );
};

export default Index;
