import React from 'react';

const data = [
  {
    name: '安全',
    title: '系统与数据',
    subtitle: 'SECURITY',
    description: '完善的安全性设计：有效保障用户权限、敏感数据等不被盗取破坏，系统稳健运行，有效避免各类注入攻击等。',
  },
  {
    name: '低价',
    title: '开发与运维',
    subtitle: 'LOW PRICE',
    description: '多种授权模式：支持按需授权无版权纠纷，比市场更廉价的解决方案，提供更有效节约时间成本和研发成本。',
  },
  {
    name: '易用',
    title: '开发与用户',
    subtitle: 'EASILY USE',
    description: '遵循用户使用习惯：无需重新学习额外技术轻松上手，完善的文档和规范，对开发和用户都提供极简的入门体验。',
  },
  {
    name: '高效',
    title: '代码于运行',
    subtitle: 'FAST EFFICIENT',
    description: '集成低代码平台：可快速完成需求开发任务，完善的扩展性与系统性能，有效提高开发效率及用户等待时间。',
  },
  {
    name: '开源',
    title: '源码与文档',
    subtitle: 'OPEN SOURCE',
    description: '100%提供源码与文档，详细的注释信息，有大厂的代码规范，代码无后门，完善的分支管理与版本发布。',
  },
  {
    name: '售后',
    title: '技术支持服务',
    subtitle: 'TECHNICAL SUPPORT',
    description: '完善的售后保障，提供1对1技术支持，有强大的技术交流平台，免费提供各类技术文档，无隐性收费。',
  },
];

interface Props {
  data?: [];
}

const Index: React.FC<Props> = (props) => {
  return (
    <section className="box-section">
      <div className="container">
        <div className="title-section">
          <h2>产品优势</h2>
          <p>
            安全、稳定、持续更新的低代码快速开发平台，节省开发时间，减少重复造轮子，提升开发效率，助力企业实现数字化经营！
          </p>
        </div>
        <ul className="grid sm:grid-cols-1 md:grid-cols-2 xl:grid-cols-3 advantage">
          {data?.map((item) => {
            return (
              <li key={item.title}>
                <i></i>
                <h2>{item.name}</h2>
                <div className="description">
                  <h4>{item.title}</h4>
                  <hr />
                  <h6>{item.subtitle}</h6>
                  <p>{item.description}</p>
                </div>
              </li>
            );
          })}
        </ul>
      </div>
    </section>
  );
};

export default Index;
