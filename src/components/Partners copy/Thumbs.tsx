import classNames from 'classnames';
import { Autoplay, Navigation, Pagination, Mousewheel, Keyboard } from 'swiper';
import { Swiper, SwiperProps } from 'swiper/react';

interface Props extends SwiperProps {
  isNavigationContainer?: boolean;
  children: React.ReactNode;
}

export default function Thumbs(props: Props) {
  const { children, isNavigationContainer = true, ...rest } = props;
  return (
    <Swiper
      className="thumbs"
      modules={[Autoplay, Navigation, Pagination, Mousewheel, Keyboard]}
      wrapperTag="section"
      slidesPerView={1}
      spaceBetween={0}
      centeredSlides
      autoplay={{
        delay: 2000,
        disableOnInteraction: false,
        pauseOnMouseEnter: true,
      }}
      loop
      {...rest}
    >
      <div className={classNames('slide-navigation-wrap', { container: isNavigationContainer })}>
        <div className="swiper-button-prev"></div>
        <div className="swiper-button-next"></div>
      </div>
      {children}
    </Swiper>
  );
}
