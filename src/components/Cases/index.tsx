import request, { setMessage } from '@/utils/request';
import { useRequest } from 'ahooks';
import { ResultDto } from '@/types/ResultDto';
import { SwiperSlide } from 'swiper/react';
import SwiperWrapper from '../SwiperWrapper';
import Loading from '../Loading';

export interface CasesData {
  image: string;
  title: string;
  url: string;
  description: string;
}

interface Props {
  t?: any;
}

const Index: React.FC<Props> = (props) => {
  const { data: resultDTO, loading } = useRequest(() => request.get<ResultDto<CasesData[]>, ResultDto<CasesData[]>>('/api/home/cases'), {
    manual: false,
  });
  const data = resultDTO?.data || [];

  return (
    <section className="recent-case-studies box-section">
      {loading && <Loading />}
      <div className="container">
        <div className="title-section">
          <h2>成功案例</h2>
          <p>无论项目大小，我们都会不遗余力的为您提供最优质的产品和高质量的服务</p>
        </div>
        {data?.length > 0 && (
          <SwiperWrapper
            className="cases-slideshow"
            slidesPerView={'auto'}
            spaceBetween={32}
            slidesPerGroup={1}
            delay={1000}
            speed={500}
            mousewheel
            // loopFillGroupWithBlank={true}
            // autoplay={false}
            centeredSlides={false}
            slidesPerGroupSkip={2}
            // grabCursor={false}
            // keyboard={{ enabled: true }}
          >
            {data?.map((item) => {
              return (
                <SwiperSlide key={item.image}>
                  <div className="single-case-item">
                    <div className="case-image">
                      <a href="#">
                        <img src={item.image} alt="case" />
                      </a>
                      <div className="overly">
                        <a href="#">Details</a>
                      </div>
                    </div>
                    <div className="case-content">
                      <h3>
                        <a href="#">{item.title}</a>
                      </h3>
                      <p>{item.description}</p>
                    </div>
                  </div>
                </SwiperSlide>
              );
            })}
          </SwiperWrapper>
        )}
      </div>
    </section>
  );
};

export default Index;
