import React, { useContext, createContext, useMemo } from 'react';
import { useSetState } from 'ahooks';
import i18next, { ReactOptions, i18n, Resource, Namespace, TypeOptions, TFunction, KeyPrefix } from 'i18next';
export interface ContexI18ntValue {
  i18n: TFunction<string, any>;
  setI18n?: (value: TFunction<string, any>) => void;
}

/** UI配置上下文 */
export const ContexI18nt = createContext<Partial<ContexI18ntValue>>({});

/** 获取UI配置的hook */
export const useContexI18nt = () => {
  const context = useContext(ContexI18nt);
  if (context) {
    return context;
  }
  return {};
};

interface Props {
  i18n: TFunction<string, any>;
  /** 子节点内容 */
  children: React.ReactElement;
}

/** UI配置上下文供应者 */
export const ContexI18ntProvider = (props: Props) => {
  const { children, ...context } = props;

  const [contextValue, setContextValue] = useSetState<Partial<ContexI18ntValue>>({
    ...context,
  });
  const value = useMemo<Partial<ContexI18ntValue>>(
    () => ({
      ...contextValue,
      setContextValue,
    }),
    [contextValue, setContextValue],
  );

  return <ContexI18nt.Provider value={value}>{children}</ContexI18nt.Provider>;
};
