import Image from 'next/image';
import { IconFont } from '@/components/Icons';

interface Props {}

const Index: React.FC<Props> = (props) => {
  return (
    <aside className="online-aside hidden lg:block">
      <div className="fold">
        <IconFont icon="double-right" />
      </div>
      <ul>
        <li>
          <IconFont icon="headset" />
          <h6>在线客服</h6>
          <div className="reight-side-hover qqlist">
            <p>
              <b>售前：</b>
              <a target="blank" href="tencent://message/?uin=118118452&site=www.yloo.cn&menu=yes">
                <img src="/images/qq-online.gif" alt="点击我发消息" />
              </a>
            </p>
            <p>
              <b>售前：</b>
              <a target="blank" href="mqqwpa://im/chat?chat_type=wpa&uin=118118452&version=1">
                <img src="/images/qq-online.gif" alt="点击我发消息" />
              </a>
            </p>
            <p>
              <b>技术：</b>
              <a target="blank" href="tencent://message/?uin=118118452&site=www.yloo.cn&menu=yes">
                <img src="/images/qq-online.gif" alt="点击我发消息" />
              </a>
            </p>
            <p>技术热线: 13666666666</p>
          </div>
        </li>
        <li>
          <IconFont icon="qq-fill" />
          <h6>技术交流</h6>
          <div className="reight-side-hover">
            <Image width={180} height={180} src="/images/qq-group-16107586.png" alt={'技术交流群'} />
            <div className="divider-text">
              <h6 className="my-2">QQ扫码进群</h6>
            </div>
            <p>QQ群1：16107586</p>
            <p>QQ群2：107596819</p>
          </div>
        </li>
        <li>
          <IconFont icon="wechat" />
          <h6>微信咨询</h6>
          <div className="reight-side-hover">
            <Image width={180} height={180} src="/images/weixin.png" alt={'商务咨询'} />
            <div className="divider-text">
              <h6 className="my-2">扫码关注</h6>
            </div>
            <p>微信号：yloo_cn</p>
          </div>
        </li>
        <li>
          <IconFont icon="message" />
          <h6>在线留言</h6>
        </li>
        <li>
          <IconFont
            icon="rocket"
            onClick={() => {
              let top = window.scrollY;
              let time = setInterval(function () {
                top -= 100;
                if (top <= 0) {
                  window.scrollTo(0, 0);
                  clearInterval(time);
                } else {
                  window.scrollTo(0, top);
                }
              }, 10);
            }}
          />
        </li>
      </ul>
    </aside>
  );
};

export default Index;
