import React from 'react';
import classNames from 'classnames';
import { FcIcon } from '@/icons/fc';

export type IconFontProps = {
  icon: FcIcon | string;
  namespace?: string;
  type?: 'svg' | 'class';
  fill?: string;
  size?: number;
  style?: React.CSSProperties;
  className?: string;
  children?: React.ReactNode;
  onClick?: React.MouseEventHandler;
};

const IconFont: React.FC<IconFontProps> = (props) => {
  const {
    icon,
    type = 'svg',
    namespace = '/fonts/fc.symbol',
    fill = 'currentcolor',
    size,
    style,
    className,
    children,
    ...rest
  } = props;
  if (type === 'svg' && namespace) {
    return (
      <>
        <svg className={classNames('svg-icon icon', className)} style={{ fontSize: size, ...style }} {...rest}>
          <use href={`${namespace}.svg#fc-${icon}`} fill={fill} />
        </svg>
        {children}
      </>
    );
  }

  return (
    <>
      <i className={classNames(`icon fc fc-${icon}`, className)} {...rest}></i>
      {children}
    </>
  );
};

export default IconFont;
