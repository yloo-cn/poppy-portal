import IconFont, { IconFontProps } from './IconFont';
import SvgIcon, { SvgIconProps } from './SvgIcon';

export default IconFont;
export { IconFont, SvgIcon };
export type { IconFontProps, SvgIconProps };
