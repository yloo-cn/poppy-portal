import React from 'react';
import classNames from 'classnames';

export type SvgIconProps = {
  /** 控制图标填充颜色 */
  fill?: string | null;
  /** SVG元素对象 */
  children: React.ReactElement;
  /** 图标类名 */
  className?: string;
  onClick?: React.MouseEventHandler;
};

const SvgIcon: React.FC<SvgIconProps> = (props) => {
  const { fill = 'currentcolor', children, className, ...rest } = props;

  return (
    <>
      {React.Children.map(children, (child) => {
        return React.cloneElement(child, {
          fill,
          className: classNames('svg-icon icon', className),
          ...rest,
        });
      })}
    </>
  );
};

export default SvgIcon;
