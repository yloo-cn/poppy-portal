import classNames from 'classnames';
import { Autoplay, Mousewheel, Navigation, Pagination, Thumbs } from 'swiper';
import { Swiper, SwiperProps } from 'swiper/react';

export interface SwiperWrapperProps extends SwiperProps {
  delay?: number;
  navigationContainer?: boolean;
  children: React.ReactNode;
}

const Index: React.FC<SwiperWrapperProps> = (props) => {
  const {
    delay = 3000,
    modules = [],
    navigation = {
      prevEl: '.swiper-button-prev',
      nextEl: '.swiper-button-next',
    },
    navigationContainer,
    children,
    ...rest
  } = props;
  return (
    <Swiper
      loop
      centeredSlides
      modules={[Autoplay, Navigation, Pagination, Mousewheel, Thumbs, ...modules]}
      tag="section"
      slidesPerView={1}
      spaceBetween={0}
      speed={500}
      navigation={navigation}
      autoplay={{
        delay,
        disableOnInteraction: false,
        pauseOnMouseEnter: true,
      }}
      pagination={{
        clickable: true,
      }}
      on={{
        init: function (swiper) {
          console.log('1', swiper);
        },
        transitionStart: function (swiper) {
          console.log('2', swiper);
        },
        transitionEnd: function (swiper) {
          console.log(swiper);
        },
        slideChangeTransitionEnd: function (swiper) {
          swiper.$el.transition(5000 + 700).transform('translate3d(-60px, 0, 0)');
        },
        slideChangeTransitionStart: function (swiper) {
          swiper.$el.transition(700).transform('translate3d(0, 0, 0)');
        },
      }}
      {...rest}
    >
      {navigation && (
        <div className={classNames('slide-navigation-wrap', { container: navigationContainer })}>
          <div className="swiper-button-prev"></div>
          <div className="swiper-button-next"></div>
        </div>
      )}
      {children}
    </Swiper>
  );
};

export default Index;
