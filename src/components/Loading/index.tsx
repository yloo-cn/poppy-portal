import { Spin } from 'antd';

const Loading = () => (
  <div className="loading-wrap">
    <Spin size="large" />
  </div>
);

export default Loading;
