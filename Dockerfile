﻿# 基于node镜像
FROM node:16-alpine

LABEL author="义陆·逍遥"
LABEL email="yloo-cn@qq.com"

# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat

# RUN mkdir -p /data
# 指定当前工作目录
WORKDIR /data


COPY . /data

# Install dependencies based on the preferred package manager
# COPY package.json yarn.lock* package-lock.json* pnpm-lock.yaml* ./
# 安装 pnpm
RUN \
  if [ -f pnpm-lock.yaml ]; then yarn global add pnpm && pnpm i --frozen-lockfile; \
  elif [ -f yarn.lock ]; then yarn --frozen-lockfile; \
  elif [ -f package-lock.json ]; then npm ci; \
  else echo "Lockfile not found." && exit 1; \
  fi

ENV PORT 3000

EXPOSE 3000

CMD ["pnpm", "start"]
