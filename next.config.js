const path = require('path');
const { i18n } = require('./next-i18next.config');
// const withTM = require('next-transpile-modules')(['antd-mobile']);

const nextConfig = {
  // 开启会导致useEffect执行2次
  // reactStrictMode: true,
  i18n,
  swcMinify: true,
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  // 环境变量
  env: {
    BASE_URL: process.env.BASE_URL,
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/i,
      issuer: /\.[jt]sx?$/,
      use: ['@svgr/webpack'],
    });

    return config;
  },
  // webpack: (config, options) => {
  //   config.module.rules.push({
  //     test: /\.svg/,
  //     use: [
  //       options.defaultLoaders.babel,
  //       {
  //         loader: '@mdx-js/loader',
  //         options: pluginOptions.options,
  //       },
  //     ],
  //   });

  //   return config;
  // },
};

// module.exports = withTM(nextConfig);
module.exports = nextConfig;
